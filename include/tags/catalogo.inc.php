<?php

Class catalogo extends TagLibrary {

    function insertcatalogo($name, $data, $pars) {
        foreach ($data as $key => $value) {
            $prezzo_int = intval($value['prezzo']);
            $prezzo_dec = intval(($value['prezzo'] - floatval($prezzo_int)) * 100);
            if ($prezzo_dec < 10) {



                $content.="<div class='product'>
    <a class='left' href='prodotto.php?id=" . $value['id'] . "' title='" . $value['titolo'] . "'><img src='img/film/" . $value['immagine'] . "' alt='" . $value['nome'] . "' /></a>
    <div class='price'>
        <div class='inner'>
            <span class='title'>Price</span>
            <strong><span>&#8364;</span>" . $prezzo_int . "<sup>.0" . $prezzo_dec . "</sup></strong>
        </div>
    </div>
    <div class='info'>
        <a href='prodotto.php?id=" . $value['id'] . "'><p class='titolo'>" . $value['titolo'] . "</p></a><br />
    </div>
</div>";
            } else {
                $content.="<div class='product'>
    <a class='left' href='prodotto.php?id=" . $value['id'] . "' title='" . $value['titolo'] . "'><img src='img/film/" . $value['immagine'] . "' alt='" . $value['nome'] . "' /></a>
    <div class='price'>
        <div class='inner'>
            <span class='title'>Price</span>
            <strong><span>&#8364; </span>" . $prezzo_int . "<sup>." . $prezzo_dec . "</sup></strong>
        </div>
    </div>
    <div class='info'>
        <a href='prodotto.php?id=" . $value['id'] . "'><p class='titolo'>" . $value['titolo'] . "</p></a><br />
    </div>
</div>";
            }
        }
        return $content;
    }

    function page($name, $data, $pars) {
        $prev1 = $data[0]['current'] - 1;
        $next1 = $data[0]['current'] + 1;
        $last=0;
        is_int($data[0]['count'] / 9)?$last = intval($data[0]['count'] / 9):$last = intval($data[0]['count'] / 9) + 1;
            
            if ($data[0]['current'] > '1') {
                    $content.="<td class='pager'><a href='/filmbuster/".$data[0]['categoria'].".php?".$data[0]['categoria']."=" . $data[0]['valore'] . "&page=" . $prev1 . "' style='text-decoration: none;'><b>«</b></a></td>";
                }
                $content.="<td class='pagebr'>&nbsp;</td><td class='highlight'><b>".$data[0]['current']."</b></td>";
                if ($data[0]['current'] == $last) {
                    
                } else {
                    $content.="<td class='pagebr'>&nbsp;</td>
                <td class='pager'><a href='/filmbuster/".$data[0]['categoria'].".php?".$data[0]['categoria']."=" . $data[0]['valore'] . "&page=" . $next1 . "' style='text-decoration: none;'><b>»</b></a></td>";
                }
                $content.="<td class='pagebr'>&nbsp;</td><td class='pager'>di " . $last . "</td>";
                
        return $content;
    }

}

?>