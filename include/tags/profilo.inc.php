<?php

function data_it($data) {
    // Creo una array dividendo la data YYYY-MM-DD sulla base del trattino
    $array = explode("-", $data);

    // Riorganizzo gli elementi in stile DD/MM/YYYY
    $data_it = $array[2] . "/" . $array[1] . "/" . $array[0];

    // Restituisco il valore della data in formato italiano
    return $data_it;
}

Class profilo extends TagLibrary {

    function inserttab($name, $data, $pars) {
        foreach ($data as $key => $value) {
            if ($data[0]['tab'] == substr($value['link'], 24)|| $data[0]['tab']==substr(($value['link']."2"),24)) {
                $content.="<li class='active'><a href='" . $value['link'] . "'><span>" . $value['nometab'] . "</span></a></li>";
            } else {                
                    $content.="<li><a href='" . $value['link'] . "'><span>" . $value['nometab'] . "</span></a></li>";
            }
        }



        return $content;
    }

    function insertcontenttab($name, $data, $pars) {
        switch ($data[0]['tab']) {
            case 'dati':
                $content.="<div class='profilotable'>
        <table>
            <tr>
                <td><span>Nome: <strong>" . $data[0]['nome'] . "</span></td>
                <td><span>Città: <strong>" . $data[0]['citta'] . "</span></td>
            </tr>
            <tr>
                <td><span>Cognome: <strong>" . $data[0]['cognome'] . "</strong></span></td>
                <td><span>Via: <strong>" . $data[0]['via'] . "</strong></span></td>
            </tr>
            <tr>
                <td><span>Data Di Nascita: <strong>" . data_it($data[0]['data_nasc']) . "</strong></span></td>
                <td><span>Provincia: <strong>" . $data[0]['provincia'] . "</strong></span></td>
            </tr>
            <tr>
                <td><span>Codice Fiscale: <strong>" . $data[0]['CF'] . "</strong></span></td>
                <td><span>CAP: <strong>" . $data[0]['cap'] . "</strong></span></td>
            </tr>
            <tr>
                <td><span>Telefono: <strong>" . $data[0]['telefono'] . "</strong></span></td>
                <td><span>Stato: <strong>" . $data[0]['paese'] . "</strong></span></td>
            </tr>
            <tr>
                <td><span>Email: <strong>" . $data[0]['email'] . "</strong></span></td>
            </tr>
        </table>
        </div>";
                break;
            case 'modificaana':
                $content.="</br>
                <div class='ana-box'>
                <form action='include/aggiorna_ana.php' method='POST' class='register' name='register' data-validate='parsley' id='mod_ana'>
                <table class='register-table'>
                <tr>
                    <th><b>Nome</b></th>
                    <th><input type='text' name='nome' value='" . $data[0]['nome'] . "' data-required='true' class='parsley-validated'></br></th>
                    <th><b>Città</b></th>
                    <th><input type='text' name='città' value='" . $data[0]['citta'] . "' data-required='true' class='parsley-validated'><br></th>

                </tr>
                <tr>
                    <th><b>Cognome</b></th>
                    <th><input type='text' name='cognome' value='" . $data[0]['cognome'] . "' data-required='true' class='parsley-validated'></br></th>
                    <th><b>Via</b></th>
                    <th><input type='text' name='via' value='" . $data[0]['via'] . "' data-required='true' class='parsley-validated'></th>

                </tr>
                <tr>
                    <th><b>Data Di Nascita</b></th>
                    <th><input type='text' data-required='true' class='parsley-validated' name='date' value='" . data_it($data[0]['data_nasc']) . "' onclick='Calendar.show(this, &#39;%d/%m/%Y&#39;, false)' onfocus='Calendar.show(this, &#39;%d/%m/%Y&#39;, true)' onblur='Calendar.hide()' /></br> </th>
                    <th><b>Provincia</b></th>
                    <th><div class='dropdown-reg'><select data-required='true' class='register-select parsley-validated' name='provincia' >";
                $oid3 = "SELECT nomep
FROM province
ORDER BY nomep ASC";
                $result3 = getResult($oid3);
                foreach ($result3 as $key => $value) {
                    if ($value['nomep'] == $data[0]['provincia'])
                        $content.="<option selected='selected'>" . $value['nomep'] . "</option>";
                    else
                        $content.="<option>" . $value['nomep'] . "</option>";
                }
                $content.="</select></div><br></th>   
                </tr>
                <tr>
                    <th><b>Codice Fiscale</b></th>
                    <th><input type='text' name='CF' value='" . $data[0]['CF'] . "' data-required='true' class='parsley-validated'></br></th>
                    <th><b>CAP</b></th>
                    <th><input type='text' name='cap' value='" . $data[0]['cap'] . "' data-type='digits' data-required='true' class='parsley-validated'><br></th>   
                </tr>
                <tr>
                    <th><b>Telefono</b></th>
                    <th><input type='text' name='telefono' value='" . $data[0]['telefono'] . "' data-type='digits' data-required='true' class='parsley-validated'><br></th> 
                    <th><b>Stato</b></th>
                    <th><div class='dropdown-reg'><select name='stato' data-required='true' class='register-select parsley-validated'>";
                $oid4 = "SELECT nome
FROM stati
ORDER BY nome ASC";
                $result4 = getResult($oid4);
                foreach ($result4 as $key => $value) {
                    if ($value['nome'] == $data[0]['paese'])
                        $content.="<option selected='selected'>" . $value['nome'] . "</option>";
                    else
                        $content.="<option>" . $value['nome'] . "</option>";
                }
                $content.="</select></div><br></th>                      
                </tr>
            </table>
            </div></br>
            <input class='tab-sub' id='submit' type='submit' value='Salva'></form>";
                break;

            case 'modificamail':
                $content.="<form action='include/aggiorna_email.php' id='mod_mail' method='POST' class='register' name='register' data-validate='parsley'>
                    <div class='reg-bottom'>            
            <table class='register-table register-table2'>
                <tr>
                    <th><b>Email attuale</b></th>
                    <th><input type='email' name='email' data-required='true' class='parsley-validated'></br></th>                    
                </tr>
                <tr>
                    <th><b>Nuova Email</b></th>
                    <th><input type='email' name='newemail' id='newemail' data-required='true' class='parsley-validated'></br></th>                       
                </tr>
                <tr>
                    <th><b>Ripeti Nuova Email</b></th>
                    <th><input type='email' name='newemail2' data-equalto='#newemail' data-required='true' class='parsley-validated'></br></th>                       
                </tr>
                <tr>
                    <th></th>
                    <th></br><input id='submit' type='submit' value='Salva'></br></th>                    
                </tr>
            </table>
        </div>
        </form>";
                break;
            case 'modificapass':
                $content.="<form action='include/aggiorna_pass.php' id='mod_pass' method='POST' class='register' name='register' data-validate='parsley'>
                    <div class='reg-bottom'>            
            <table class='register-table register-table2'>
                <tr>
                    <th><b>Password attuale</b></th>
                    <th><input type='password' name='pass' data-required='true' class='reg-pass parsley-validated'></br></th>                    
                </tr>
                <tr>
                    <th><b>Nuova Password</b></th>
                    <th><input type='password' name='newpass' id='pass1' data-required='true' class='reg-pass parsley-validated'></br></th>                       
                </tr>
                <tr>
                    <th><b>Ripeti Nuova Password</b></th>
                    <th><input type='password' name='newpass2' data-equalto='#pass1' data-required='true' class='reg-pass parsley-validated'></br></th>                       
                </tr>
                <tr>
                    <th></th>
                    <th></br><input id='submit' type='submit' value='Salva'></br></th>                    
                </tr>
            </table>
        </div>
        </form>";
                break;
            case 'additem':
                $qcnt = "SELECT count(id) AS cnt
FROM prodotti";
                $cnt = getResult($qcnt);
                $content.="</br>
                    <h1>Prodotti in database: " . $cnt[0]['cnt'] . "</h1>
                    </br>
                <div class='ana-box'>
                <form action='include/aggiungiprod.php' id='additem' method='POST' class='register' name='register' data-validate='parsley'>
                <table class='register-table'>
                <tr>
                    <th><b>Titolo</b></th>
                    <th><input type='text' name='titolo' value='' data-required='true' class='parsley-validated'></br></th>
                    <th><b>Durata (min)</b></th>
                    <th><input type='text' name='durata'  data-required='true' class='parsley-validated'><br></th>

                </tr>
                <tr>
                    <th><b>Genere</b></th>
                    <th><div class='dropdown-reg'><select name='genere' data-required='true' class='register-select parsley-validated'>";
                $oid6 = "SELECT nome
FROM genere
ORDER BY nome ASC";
                $result6 = getResult($oid6);
                foreach ($result6 as $key => $value) {
                    $content.="<option>" . $value['nome'] . "</option>";
                }
                $content.="</select></div><br></th> 
                    <th><b>Attori</b></th>
                    <th><input type='text' name='attori' data-required='true' class='parsley-validated'/></br> </th>
                </tr>
                <tr>
                    <th><b>Regia</b></th>
                    <th><input type='text' name='regia' data-required='true' class='parsley-validated'/></br> </th>
                    <th><b>Trama</b></th>
                    <th><input type='text' name='trama' data-required='true' class='parsley-validated'><br></th>
                </tr>
                <tr>
                    <th><b>Anno</b></th>
                    <th><div class='dropdown-reg'><select name='anno' data-required='true' class='register-select parsley-validated'>";
                $oid4 = "SELECT anno
FROM anno
ORDER BY anno ASC";
                $result4 = getResult($oid4);
                foreach ($result4 as $key => $value) {
                    $content.="<option>" . $value['anno'] . "</option>";
                }
                $content.="</select></div><br></th>                        
                    <th><b>Quantità</b></th>
                    <th><input type='text' name='qnt' data-required='true' class='parsley-validated'></th>
                </tr>
                <tr>
                    <th><b>Nazione</b></th>
                    <th><div class='dropdown-reg'><select name='nazione' data-required='true' class='register-select parsley-validated'>";
                $oid5 = "SELECT nome
FROM stati
ORDER BY nome ASC";
                $result5 = getResult($oid5);
                foreach ($result5 as $key => $value) {
                    $content.="<option>" . $value['nome'] . "</option>";
                }
                $content.="</select></div><br></th> 
                    <th><b>Prezzo</b></th>
                    <th><input type='text' name='prezzo' data-required='true' class='parsley-validated'/></br> </th>                      
                </tr>
            </table>
            </div></br>
            <input class='tab-sub' id='submit' type='submit' value='Aggiungi Prodotto'></form>";
                break;

            case 'deleteitem':
                $qcnt = "SELECT count(id) AS cnt
FROM prodotti";
                $cnt = getResult($qcnt);
                $content.="</br>
                    <div id=delete>
                    <h1>Prodotti in database: " . $cnt[0]['cnt'] . "</h1></br></br>
                    <h1>Selezionare il film da eliminare</h1>
                    <h3>( Attention: cannot be undone )</h3>
                    </div>
                    </br>
                
                <form onsubmit='check_deleteitem()' method='POST' class='register' name='register'>
                <table class='register-table'><tr>
                
                    
                    <th><div class='dropdown-reg delete'><select class='register-select' name='titolo'>";
                $oid6 = "SELECT titolo, id
FROM prodotti
ORDER BY titolo ASC";
                $result6 = getResult($oid6);
                foreach ($result6 as $key => $value) {
                    $content.="<option>" . $value['titolo'] . " (id." . $value['id'] . ")</option>";
                }
                $content.="</select></div><br></th> 
                    
                </tr>
                
            </table></br>
            <input id='del-sub' type='submit' value='Cancella Prodotto'></form>";
                break;

            case 'modprod':
                $content.="</br>
                    <div id=delete>
                    <h1>Selezionare il film da modificare</h1>
                    </div>
                    </br>
                
                <form action='include/modprod.php' method='POST' class='register' data-validate='parsley' >
                <table class='register-table'><tr>
                
                    
                    <th><div class='dropdown-reg delete'><select class='register-select' name='titolo'>";
                $oid6 = "SELECT titolo, id
FROM prodotti
ORDER BY titolo ASC";
                $result6 = getResult($oid6);
                foreach ($result6 as $key => $value) {
                    $content.="<option>" . $value['titolo'] . " (id." . $value['id'] . ")</option>";
                }
                $content.="</select></div><br></th> 
                    
                </tr>
                
            </table></br>
            <input id='del-sub' type='submit' value='Modifica Prodotto'></form>";
                break;
                
                case 'modprod2':
                    $oid9 = "SELECT *
FROM prodotti WHERE id='".$_GET['id']."'";
                $result9 = getResult($oid9);
                $content.="</br>
                <form action='include/concludimodprod.php' id='mod_prod' method='POST' class='register' name='register' data-validate='parsley'> 
                    <h1>Modifica del film con id <input readonly='text' id='filmid' type='text' name='id' value='".$result9[0]['id']."'/> </h1></br>                  

                <div class='ana-box'>
                <table class='register-table'>
                <tr>
                    <th><b>Titolo</b></th>
                    <th><input type='text' name='titolo' value='".$result9[0]['titolo']."' data-required='true' class='parsley-validated'></br></th>
                    <th><b>Durata (min)</b></th>
                    <th><input type='text' name='durata' value='".$result9[0]['durata']."' data-required='true' class='parsley-validated'><br></th>

                </tr>
                <tr>
                    <th><b>Genere</b></th>
                    <th><div class='dropdown-reg'><select name='genere' data-required='true' class='register-select parsley-validated'>";
                $oid6 = "SELECT nome
FROM genere
ORDER BY nome ASC";
                $result6 = getResult($oid6);
                foreach ($result6 as $key => $value) {
                    if($value['nome']==$result9[0]['genere']){
                        $content.="<option selected='selected'>" . $value['nome'] . "</option>";
                    }
                    else $content.="<option>" . $value['nome'] . "</option>";
                }
                $content.="</select></div><br></th> 
                    <th><b>Attori</b></th>
                    <th><input type='text' name='attori' value='".$result9[0]['attori']."' data-required='true' class='parsley-validated'/></br> </th>
                </tr>
                <tr>
                    <th><b>Regia</b></th>
                    <th><input type='text' name='regia' value='".$result9[0]['regia']."' data-required='true' class='parsley-validated'/></br> </th>
                    <th><b>Trama</b></th>
                    <th><input type='text' name='trama' value='".$result9[0]['trama']."' data-required='true' class='parsley-validated'><br></th>
                </tr>
                <tr>
                    <th><b>Anno</b></th>
                    <th><div class='dropdown-reg'><select name='anno' data-required='true' class='register-select parsley-validated'>";
                $oid4 = "SELECT anno
FROM anno
ORDER BY anno ASC";
                $result4 = getResult($oid4);
                foreach ($result4 as $key => $value) {
                    if($value['anno']==$result9[0]['anno']){
                        $content.="<option selected='selected'>" . $value['anno'] . "</option>";
                    }
                    else $content.="<option>" . $value['anno'] . "</option>";
                }
                $content.="</select></div><br></th>                        
                    <th><b>Quantità</b></th>
                    <th><input type='text' name='qnt' value='".$result9[0]['quantita']."' data-required='true' class='parsley-validated'></th>
                </tr>
                <tr>
                    <th><b>Nazione</b></th>
                    <th><div class='dropdown-reg'><select name='nazione' data-required='true' class='register-select parsley-validated'>";
                $oid5 = "SELECT nome
FROM stati
ORDER BY nome ASC";
                $result5 = getResult($oid5);
                foreach ($result5 as $key => $value) {
                    if($value['nome']==ucwords(strtolower($result9[0]['nazione']))){
                        $content.="<option selected='selected'>" . $value['nome'] . "</option>";
                    }
                    else $content.="<option>" . $value['nome'] . "</option>";
                }
                $content.="</select></div><br></th> 
                    <th><b>Prezzo</b></th>
                    <th><input type='text' name='prezzo' value='".$result9[0]['prezzo']."' data-required='true' class='parsley-validated'/></br> </th>                      
                </tr>
            </table>
            </div></br>
            <input class='tab-sub' id='submit' type='submit' value='Modifica Prodotto'></form>";
                break;


            case 'addute':
                $qcnt = "SELECT count(email) AS cnt
FROM utenti";
                $cnt = getResult($qcnt);

                $content.="</br><h1>Utenti registrati: " . $cnt[0]['cnt'] . "</h1></br></br>
                <form action='include/registerutente.php?id=admin' id='add_ute' method='POST' class='register' name='register' data-validate='parsley'>
                <div class='ana-box'>
                <h1>Anagrafica</h1>                
                <table class='register-table'>
                <tr>
                    <th><b>Nome</b></th>
                    <th><input type='text' name='nome' value='' data-required='true' class='parsley-validated'></br></th>
                    <th><b>Città</b></th>
                    <th><input type='text' name='città' value='' data-required='true' class='parsley-validated'><br></th>

                </tr>
                <tr>
                    <th><b>Cognome</b></th>
                    <th><input type='text' name='cognome' value='' data-required='true' class='parsley-validated'></br></th>
                    <th><b>Via</b></th>
                    <th><input type='text' name='via' value='' data-required='true' class='parsley-validated'></th>

                </tr>
                <tr>
                    <th><b>Data Di Nascita</b></th>
                    <th><input type='text' data-required='true' class='parsley-validated' name='date' value='' onclick='Calendar.show(this, &#39;%d/%m/%Y&#39;, false)' onfocus='Calendar.show(this, &#39;%d/%m/%Y&#39;, true)' onblur='Calendar.hide()' /></br> </th>
                    <th><b>Provincia</b></th>
                    <th><div class='dropdown-reg'><select name='provincia' data-required='true' class='register-select parsley-validated' >";
                $oid3 = "SELECT nomep
FROM province
ORDER BY nomep ASC";
                $result3 = getResult($oid3);
                foreach ($result3 as $key => $value) {
                    $content.="<option>" . $value['nomep'] . "</option>";
                }
                $content.="</select></div><br></th>   
                </tr>
                <tr>
                    <th><b>Codice Fiscale</b></th>
                    <th><input type='text' name='CF' value='' data-required='true' class='parsley-validated'></br></th>
                    <th><b>CAP</b></th>
                    <th><input type='text' name='cap' value='' data-required='true' data-type='digits' class='parsley-validated'><br></th>   
                </tr>
                <tr>
                    <th><b>Telefono</b></th>
                    <th><input type='text' name='telefono' value='' data-type='digits' data-required='true' class='parsley-validated'><br></th> 
                    <th><b>Stato</b></th>
                    <th><div class='dropdown-reg'><select name='stato' data-required='true' class='register-select parsley-validated'>";
                $oid4 = "SELECT nome
FROM stati
ORDER BY nome ASC";
                $result4 = getResult($oid4);
                foreach ($result4 as $key => $value) {
                    $content.="<option>" . $value['nome'] . "</option>";
                }
                $content.="</select></div><br></th>                      
                </tr>
            </table>
            </div><div class='reg-bottom'>            
            <table class='register-table register-table2'>
            <tr>
                    <th><b>Ruolo</b></th>
                    <th><div class='dropdown-reg'><select name='ruolo' data-required='true' class='register-select parsley-validated'>";
                $oid8 = "SELECT nome_gruppo
FROM gruppo";
                $result8 = getResult($oid8);
                foreach ($result8 as $key => $value) {
                    $content.="<option>" . $value['nome_gruppo'] . "</option>";
                }
                $content.="</select></div><br></th>                      
                </tr>                    
                </tr>
                <tr>
                    <th><b>Email</b></th>
                    <th><input type='email' name='email' data-required='true' class='parsley-validated'></br></th>                    
                </tr>
                <tr>
                    <th><b>Password</b></th>
                    <th><input type='password' name='pass1' data-required='true' id='pass1' class='reg-pass parsley-validated'></br></th>                       
                </tr>                
                <tr>
                    <th><b>Ripeti Password</b></th>
                    <th><input type='password' name='pass2' data-required='true' data-equalto='#pass1' class='reg-pass parsley-validated'><br></br></th>                     
                </tr>
                <tr>
                    <th></th>
                    <th><input id='submit' type='submit' value='Registra Utente'><br></th>                    
                </tr>
            </table>
        </div>
    </form></br>";
                break;
            case 'deleteute':
                $qcnt = "SELECT count(email) AS cnt
FROM utenti";
                $cnt = getResult($qcnt);
                $content.="</br>
                    <div id=delete>
                    <h1>Utenti registrati: " . $cnt[0]['cnt'] . "</h1></br></br>
                    <h1>Selezionare l&#39;utente da eliminare</h1>
                    <h3>( Attention: cannot be undone )</h3>
                    </div>
                    </br>
                
                <form onsubmit='check_deleteute()' method='POST' class='register' name='register'>
                <table class='register-table'><tr>
                
                    
                    <th><div class='dropdown-reg delete'><select class='register-select' name='nome'>";
                $oid6 = "SELECT nome, cognome, email, id_gruppo
FROM utenti
ORDER BY nome ASC";
                $result6 = getResult($oid6);
                foreach ($result6 as $key => $value) {
                    if ($value['id_gruppo'] != 2) {
                        $oid7 = "SELECT nome_gruppo
FROM gruppo
WHERE id_gruppo='" . $value['id_gruppo'] . "'";
                        $result7 = getResult($oid7);
                        $content.="<option>" . $value['nome'] . " " . $value['cognome'] . " [" . $result7[0]['nome_gruppo'] . "] (" . $value['email'] . ")</option>";
                    }
                }
                $content.="</select></div><br></th> 
                    
                </tr>
                
            </table></br>
            <input id='del-sub' type='submit' value='Cancella Utente'></form>";
                break;
                
                case 'addcat':
                $content.="</br>
                    <div id=delete>
                    <h1>Inserire da categoria da aggiungere:</h1></br>
                    </div>
                    </br>
                
                <form action='include/addcat.php' method='POST' id='add_cat' class='register' name='register' data-validate='parsley'>
                <table class='register-table'><tr>
                
                    
                    <th><input type='text' name='nome' data-required='true' ><br></th> 
                    
                </tr>
                
            </table></br>
            <input id='del-sub' type='submit' value='Inserisci Categoria'></form>";
                break;
                
            case 'ordini':
                $content.="</br>
                    <form action='include/aggiornaordini.php' method='POST' class='register'>
                <h1>ORDINI</h1></br>
                <button type='submit' title='Aggiorna Ordini' class='btn-update'><span><span>Aggiorna Ordini</span></span></button></br></br>                
                <table class='ordini-table'>
                <tr>
                    <th class='nordine'><b>Numero Ordine</b></th>
                    <th><b>Eseguito da</b></th>
                    <th><b>Titolo</b></th>
                    <th><b>Quantità</b></th>
                    <th><b>Importo</b></th>
                    <th><b>Subtotale</b></th>
                    <th><b>Indirizzo Spedizione</b></th>
                    <th><b>Pagamento</b></th>
                    <th><b>Spedizione</b></th>
                    <th class='completed'><b>Completato</b></th>
                </tr>";
                $oid8 = "SELECT *
FROM ordini
ORDER BY stato ASC, n_ordine ASC";
                $result8 = getResult($oid8);
                foreach ($result8 as $key => $value) {
                    $spedizione = $value['spedizione'] == 0 ? 'ordinaria' : ($value['spedizione'] == 1 ? 'prioritaria' : 'corriere');
                    $pagamento = $value['pagamento'] == 0 ? 'carta' : ($value['pagamento'] == 1 ? 'bonifico' : ($value['pagamento'] == 2 ? 'contrassegno' : 'paypal'));
                    $subtotale = $value['importo'] * floatval($value['quantita']);
                    $content.="<tr><td>" . $value['n_ordine'] . "</td>
                            <td>" . $value['email'] . "</td>
                                <td>" . $value['titolo'] . "</td>
                                    <td>" . $value['quantita'] . "</td>
                                        <td>" . $value['importo'] . " &euro;</td>
                                            <td>" . $subtotale . " &euro;</td>
                                <td>" . $value['indirizzo_spedizione'] . "</td>
                                    <td>" . $pagamento . "</td>
                                        <td>" . $spedizione . "</td>
                                    <td>";
                    if ($value['stato'] == 0)
                        $content.="<input class='css-checkbox' type='checkbox' id='" . $value['id'] . "' name='stato" . $value['n_ordine'] . "'/>
                            <label for='" . $value['id'] . "' class='css-label'></label>";
                    else
                        $content.="<input class='css-checkbox' type='checkbox' checked='checked' checked disabled id='" . $value['id'] . " name='" . $value['n_ordine'] . "'/>
                            <label for='" . $value['id'] . "' class='css-label'></label>";
                    $content.="</td></tr>";
                }
                $content.="
            </table></br>
            <button type='submit' title='Aggiorna Ordini' class='btn-update'><span><span>Aggiorna Ordini</span></span></button>
    </form></br>";
                break;
        }



        return $content;
    }

}

?>
