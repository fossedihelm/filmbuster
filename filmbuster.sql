-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Lug 20, 2013 alle 16:15
-- Versione del server: 5.5.20
-- Versione PHP: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `filmbuster`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `anno`
--

CREATE TABLE IF NOT EXISTS `anno` (
  `anno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `anno`
--

INSERT INTO `anno` (`anno`) VALUES
(2000),
(2001),
(2002),
(2003),
(2004),
(2005),
(2006),
(2007),
(2008),
(2009),
(2010),
(2011),
(2012),
(2013);

-- --------------------------------------------------------

--
-- Struttura della tabella `genere`
--

CREATE TABLE IF NOT EXISTS `genere` (
  `nome` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `genere`
--

INSERT INTO `genere` (`nome`) VALUES
('Avventura'),
('Azione'),
('Classici'),
('Comici'),
('Commedia'),
('Erotici'),
('Fantascienza'),
('Guerra'),
('Horror'),
('Thriller'),
('Western'),
('Animazione'),
('');

-- --------------------------------------------------------

--
-- Struttura della tabella `gruppo`
--

CREATE TABLE IF NOT EXISTS `gruppo` (
  `id_gruppo` int(16) NOT NULL,
  `nome_gruppo` varchar(16) NOT NULL,
  PRIMARY KEY (`id_gruppo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `gruppo`
--

INSERT INTO `gruppo` (`id_gruppo`, `nome_gruppo`) VALUES
(1, 'cliente'),
(2, 'amministratore'),
(3, 'dealer');

-- --------------------------------------------------------

--
-- Struttura della tabella `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `nome` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `menu`
--

INSERT INTO `menu` (`nome`) VALUES
('home'),
('categorie'),
('anno'),
('a-z'),
('cerca'),
('contatti');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE IF NOT EXISTS `ordini` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `n_ordine` bigint(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `titolo` varchar(250) NOT NULL,
  `quantita` int(11) NOT NULL,
  `importo` float NOT NULL,
  `indirizzo_spedizione` varchar(500) NOT NULL,
  `pagamento` smallint(6) NOT NULL,
  `spedizione` smallint(6) NOT NULL,
  `stato` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dump dei dati per la tabella `ordini`
--

INSERT INTO `ordini` (`id`, `n_ordine`, `email`, `titolo`, `quantita`, `importo`, `indirizzo_spedizione`, `pagamento`, `spedizione`, `stato`) VALUES
(1, 759, 'pippo@gmail.it', 'House at the End of the Street', 2, 0, 'via pippo,13 - l''aquila 67100 L''aquila', 0, 0, 1),
(2, 555, 'pippo@gmail.it', 'Lo Hobbit - Un viaggio inaspettato', 1, 0, 'via pippo,13 - l''aquila 67100 L''aquila', 0, 0, 1),
(3, 759, 'pippo@gmail.it', 'Lo Hobbit - Un viaggio inaspettato', 4, 0, 'via pippo,13 - l''aquila 67100 L''aquila', 0, 0, 1),
(12, 34433, 'guerino@gmail.com', 'The lone ranger', 1, 9.9, '   -    -  ', 0, 0, 1),
(13, 34433, 'guerino@gmail.com', 'Cloud Atlas ', 3, 12, '   -    -  ', 0, 0, 1),
(14, 60010, 'guerino@gmail.com', 'Dead Man Down ', 1, 25, '   -    -  ', 3, 2, 1),
(15, 43597, 'guerino@gmail.com', 'Amanti perduti', 1, 19.9, 'Federico FossemÃ² das - asdas 67100 L&#039;Aquila - L&#039;Aquila 3280821334', 2, 1, 1),
(16, 42917, 'guerino@gmail.com', 'Burt Wonderstone ', 1, 10, 'Federico FossemÃ² vasva - vava 67100 L&#039;Aquila - Afghanistan 3280821334', 0, 0, 1),
(17, 69892, 'guerino@gmail.com', 'G.I. Joe - La vendetta ', 1, 19, 'Federico FossemÃ² asds - asda 67100 L&#039;Aquila - Afghanistan 3280821334', 1, 0, 1),
(18, 48682, 'fede@gmail.com', 'Comic Movie - Movie 43 ', 4, 15, 'fasf dasd fasdfa - afsdas dasda Bari - Antartide fasdfas', 0, 2, 1),
(19, 57547, 'fede@gmail.com', 'Amore e guerra', 1, 29.9, 'FasdA FASF DASDAS - FAS FAS Agrigento - Afghanistan FASDAS', 0, 2, 1),
(20, 57547, 'fede@gmail.com', 'Amanti perduti', 3, 19.9, 'FasdA FASF DASDAS - FAS FAS Agrigento - Afghanistan FASDAS', 0, 2, 1),
(21, 83268, 'alfonso@gmail.com', 'I cancelli del cielo', 3, 9.9, 'Alfonso Pierantonio Vetoio, 8 - L&#039;Aquila 67100 L&#039;Aquila - Italia 3334845123', 0, 2, 0),
(22, 83268, 'alfonso@gmail.com', 'Amanti perduti', 1, 19.9, 'Alfonso Pierantonio Vetoio, 8 - L&#039;Aquila 67100 L&#039;Aquila - Italia 3334845123', 0, 2, 0),
(23, 91440, 'angelini@gmail.com', 'Gangster Squad ', 3, 10.9, 'Francesco Angelini corridoni, 8 - Sora 02048 Frosinone - Italia 3357489565', 1, 1, 0),
(24, 91440, 'angelini@gmail.com', 'The Ghostmaker ', 1, 12, 'Francesco Angelini corridoni, 8 - Sora 02048 Frosinone - Italia 3357489565', 1, 1, 0),
(25, 91440, 'angelini@gmail.com', 'Cercasi amore per la fine del mondo ', 1, 25, 'Francesco Angelini corridoni, 8 - Sora 02048 Frosinone - Italia 3357489565', 1, 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `prezzi`
--

CREATE TABLE IF NOT EXISTS `prezzi` (
  `id_fascia` int(25) NOT NULL AUTO_INCREMENT,
  `nome` varchar(5) NOT NULL,
  `fascia` varchar(25) NOT NULL,
  PRIMARY KEY (`id_fascia`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dump dei dati per la tabella `prezzi`
--

INSERT INTO `prezzi` (`id_fascia`, `nome`, `fascia`) VALUES
(1, '010', 'da 0&euro; a 10&euro;'),
(2, '1015', 'da 10&euro; a 15&euro;'),
(3, '1520', 'da 15&euro; a 20&euro;'),
(4, '2025', 'da 20&euro; a 25&euro;'),
(5, '25', 'da 25&euro; in su');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti`
--

CREATE TABLE IF NOT EXISTS `prodotti` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `titolo` varchar(250) NOT NULL,
  `immagine` varchar(250) NOT NULL,
  `prezzo` decimal(11,2) NOT NULL,
  `rank` int(4) NOT NULL DEFAULT '0',
  `anno` int(4) NOT NULL,
  `nazione` varchar(50) NOT NULL,
  `durata` int(4) NOT NULL,
  `genere` varchar(50) NOT NULL,
  `regia` varchar(50) NOT NULL,
  `trama` longtext NOT NULL,
  `attori` varchar(250) NOT NULL,
  `quantita` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=156 ;

--
-- Dump dei dati per la tabella `prodotti`
--

INSERT INTO `prodotti` (`id`, `titolo`, `immagine`, `prezzo`, `rank`, `anno`, `nazione`, `durata`, `genere`, `regia`, `trama`, `attori`, `quantita`) VALUES
(45, 'House at the End of the Street ', 'house_at_the_end_of_the_street.jpg', '46.00', 0, 2012, 'ITALIA', 70, 'horror', 'Mark Tonderai ', 'Madre e figlia si trasferiscono nei pressi di una casa che fu teatro di un massacro. Nell`abitazione vive l`unico superstite della strage, il ragazzo emarginato dalla comunita`  attirera`  l`attenzione della sua graziosa vicina di casa che s`infatuera`  di lui.', 'Jennifer Lawrence', 38),
(44, 'Hitchcock ', 'hitchcock.jpg', '25.00', 0, 2012, 'FRANCIA', 110, 'thriller', 'Sacha Gervasi ', 'Genesi e produzione del thriller Psycho, capolavoro indiscusso del maestro del brivido Alfred Hitchcok.', 'Anthony Hopkins, Scarlett Johansson, Jessica Biel, Helen Mirren, Ralph Macchio, Toni Collette ', 36),
(43, 'Aiuto vampiro', 'aiuto_vampiro.jpg', '29.90', 0, 2000, 'USA', 105, 'thriller', 'Paul Weitz', 'Il 14enne Darren è un ragazzo come un altro del suo quartiere residenziale in una città americana. Ha buoni amici, è bravo a scuola e tende a non mettersi nei guai. Quando però lui e il suo amico Steve s''imbattono in un freak show itinerante, tutto è destinato a cambiare. Darren e Steve visitano la bizzarra manifestazione, e di fronte a donne barbute e lupi mannari ridono pensando che si tratti di una grande beffa. Ad un certo punto i due si imbattono in un vero vampiro, Larten Crepsley che chiede a Steve di diventare suo assistente, ma il ragazzo rifiuta. Il giorno dopo i due ritornano al circo, Steve viene morso dal ragno velenoso di Larten e Darren chiede al vampiro di salvare il suo amico. Larten accetta a patto che Darren diventi il suo apprendista..', 'Chris Massoglia', 34),
(42, 'Il grande Gatsby ', 'il_grande_gatsby.jpg', '16.50', 0, 2010, 'ITALIA', 100, 'avventura', 'Baz Luhrmann ', 'Nuovo adattamento in 3D del classico letterario di F. Scott Fitzgerald che racconta la storia di un aspirante scrittore, Nick Carraway che lasciato il Midwest Americano, arriva a New York nella primavera del 1922 in cerca del suo personale Sogno Americano.', 'Leonardo Di Caprio, Carey Mulligan, Tobey Maguire, Isla Fisher, Joel Edgerton ', 32),
(41, 'Il grande e potente Oz ', 'il_grande_e_potente_oz.jpg', '25.00', 0, 2011, 'FRANCIA', 95, 'fantascienza', 'Sam Raimi ', 'Oscar Diggs (James Franco) illusionista dalla discutibile etica impiegato in un piccolo circo viene trasportato dal Kansas nel fantastico Regno di Oz, qui dopo aver assaporato fama e fortuna dovra`  vederesela con tre streghe ben poco convinte che lui sia il grande mago che afferma di essere.', 'James Franco, Michelle Williams, Mila Kunis, Rachel Weisz ', 30),
(40, 'The Ghostmaker ', 'the_ghostmaker.jpg', '12.00', 0, 2012, 'USA', 90, 'horror', 'Mauro Borrelli ', 'Un gruppo di studenti utilizza una misteriosa bara per riuscire ad oltrepassare i confini della morte, le conseguenze saranno devsatanti.', 'Aaron Dean Eisenberg, Liz Fenning ', 28),
(39, 'Ghost Movie ', 'ghost_movie.jpg', '29.00', 0, 2012, 'ITALIA', 85, 'commedia', 'Michael Tiddes ', 'Quando Malcolm e Kisha si trasferiscono nella loro casa dei sogni, scoprono di non essere soli. La casa è infestata e Kisha viene posseduta da uno spirito. Malcom dovra`  farsi aiutare da un prete e da alcuni acchiappafantasmi per sbarazzarsi del demone e salvare la sua vita matrimoniale e non soloâ€¦', 'Marlon Wayans, Essence Atkins, David Koechner, Cedric The Entertainer ', 26),
(38, 'Gangster Squad ', 'gangster_squad.jpg', '10.90', 0, 2000, 'FRANCIA', 130, 'azione', 'Ruben Fleischer ', 'Los Angeles anni â€˜40, il dipartimento di polizia crea una speciale task-force per contrastare lo strapotere del boss Mickey Cohen.', 'Ryan Gosling, Emma Stone, Sean Penn, Josh Brolin, Anthony Mackie, Nick Nolte ', 24),
(37, 'G.I. Joe - La vendetta ', 'g.i._joe_-_la_vendetta.jpg', '19.00', 0, 2010, 'USA', 120, 'azione', 'Jon Chu ', 'In questo sequel in 3D i G.I. Joe non solo combattono contro i loro nemici mortali Cobra, ma sono costretti contrastare una minaccia proveniente dall`interno del governo che mette in pericolo la loro esistenza.', 'Channing Tatum, Bruce Willis, Dwayne Johnson, Ray Stevenson, Adrianne Palicki, Ray Park ', 22),
(36, 'Gambit ', 'gambit.jpg', '25.00', 0, 2011, 'ITALIA', 110, 'commedia', 'Michael Hoffman ', 'Un curatore dâ€?arte britannico (Firth) escogita una truffa per ingannare un ricco collezionista d`arte spingendolo all`acquisto un falso dipinto di Monet, ad aiutarlo una bella campionessa di rodeo texana (Diaz).', 'Colin Firth, Cameron Diaz, Stanley Tucci, Alan Rickman ', 20),
(35, 'Frozen ', 'frozen.jpg', '12.00', 0, 2000, 'FRANCIA', 100, 'animazione', 'Chris Buck e Jennifer Lee ', 'Quando una profezia intrappola un intero regno in un inverno senza fine, Anna, una temeraria sognatrice, insieme al coraggioso uomo di montagna Kristoff e alla sua renna Sven, intraprende un viaggio epico alla ricerca della sorella Elsa, la Regina delle Nevi, per riuscire a porre fine al glaciale incantesimo.', 'Kristen Bell, Idina Menzel ', 67),
(34, 'Fast & Furious 6 ', 'fast_&_furious_6.jpg', '5.90', 0, 2010, 'USA', 90, 'azione', 'Justin Lin ', 'Dominc Toretto e la sua squadra si recano in Europa per un colpo che pera`² ha attirato l`attenzione di un altro team di rapinatori, saranno scintille.', 'Vin Diesel, Paul Walker, Dwayne Johnson, Michelle Rodriguez, Jordana Brewster, Tyrese Gibson ', 62),
(33, 'Frankenweenie ', 'frankenweenie.jpg', '10.00', 0, 2011, 'ITALIA', 180, 'horror', 'Tim Burton ', 'Victor è un ragazzino che perde il suo amato cagnolino Sparky investito da un`automobile. Disperato per la perdita Il ragazzino ricorrera`  alla sua passione per la scienza per riportarlo in vita.', 'Winona Ryder, Catherine O`Hara, Martin Short, Martin Landau ', 57),
(32, 'Flight ', 'flight.jpg', '46.00', 0, 2012, 'FRANCIA', 175, 'thriller', 'Robert Zemeckis ', 'Un esperto pilota di linea evita una catastrofe ad alta quota riuscendo a far atterrare il suo aereo, salvando cosa`¬ le vite di quasi tutti i passeggeri a bordo. Mentre media e opinione pubblica lo eleggono ad eroe una commissione d`inchiesta porta alla luce troppe domande senza risposta che incombono sull`accaduto.', 'Denzel Washington, James Badge Dale, John Goodman, Don Cheadle, Melissa Leo ', 52),
(31, 'Epic - Il mondo segreto di Moonhaven ', 'epic_-_il_mondo_segreto_di_moonhaven.jpg', '25.00', 0, 2011, 'USA', 170, 'animazione', 'Chris Wedge ', 'Quando una ragazza adolescente si ritrova magicamente trasportata in un universo segreto, dovra`  unirsi ad una squadra di personaggi divertenti e stravaganti al fine di salvare il loro mondoâ€¦e il nostro.', '', 47),
(30, 'Ender`s Game ', 'ender`s_game.jpg', '15.00', 0, 2012, 'ITALIA', 165, 'fantascienza', 'Gavin HoodAsa Butterfield ', 'In un futuro prossimo una bellicosa razza aliena attacca la Terra, per questo il Colonnello Graff recluta per la sua Scuola di Guerra i pia`¹ promettenti ragazzi del pianeta. Tra questi c`è il timido Ender Wiggin, un ragazzo che dimostrera`  doti inaspettate.', 'Harrison Ford, Ben Kingsley, Viola Davis', 42),
(29, 'Elysium ', 'elysium.jpg', '10.00', 0, 2012, 'FRANCIA', 160, 'fantascienza', 'Neill Blomkamp ', 'Nell`anno 2159 le persone molto ricche vivono in un ambiente incontaminato ricreato dall`uomo su una stazione spaziale chiamata Elysium e il resto della popolazione è invece costretta su una Terra sovrappopolata e in rovina. Quando Max (Matt Damon) decidera`  di raggiungere Elysium si imbarchera`  in una missione disperata che in caso di successo non solo salvera`  la sua vita, ma potrebbe portare la parita`  in questi mondi agli antipodi.', 'Matt Damon, Jodie Foster, William Fichtner', 37),
(28, 'Die Hard - Un buon giorno per morire ', 'die_hard_-_un_buon_giorno_per_morire.jpg', '26.00', 0, 2012, 'USA', 155, 'azione', 'John Moore ', 'Torna Bruce Willis nei panni del superpoliziotto John McClane, in questo quinto capitolo McClane è in trasferta in Russia per aiutare il figlio ribelle Jack, inconsapevole che questi è in realta`  un agente operativo della CIA che sta cercando di sventare una rapina che vede come obiettivo delle armi nucleari.', 'Bruce Willis, Jai Courtney', 32),
(27, 'Dead Man Down ', 'dead_man_down.jpg', '25.00', 0, 2000, 'ITALIA', 150, 'thriller', 'Niels Arden Oplev ', 'Victor è il braccio destro di un boss del crimine che ha causato la morte di sua moglie e di sua figlia. L`uomo in cerca di vendetta trovera`  sulla sua strada la tormentata Beatrice, anche lei con un conto da saldare con il boss sedurra`  e ricattera`  Victor costringendolo ad aiutarla.', 'Noomi Rapace, Colin Farrell', 27),
(26, 'Django Unchained ', 'django_unchained.jpg', '16.00', 0, 2010, 'FRANCIA', 145, 'western', 'Quentin Tarantino ', 'Lo schiavo liberato Django addestrato dal cacciatore di taglie Schultz cerca vendetta e intraprende un viaggio per trovare e liberare la moglie Broomhilda.', 'Jamie Foxx, Leonardo Di Caprio', 22),
(25, 'Cobu 3D ', 'cobu_3d.jpg', '12.00', 0, 2011, 'USA', 200, 'commedia', 'Duane Adler ', 'Romeo e Giulietta in salsa dance. Nei sobborghi di New York due ballerini che appartengono a club di danza rivali si innamorano creando un inevitabile conflitto.', 'Derek Hough, Boa Kwon ', 17),
(24, 'Comic Movie - Movie 43 ', 'comic_movie_-_movie_43.jpg', '15.00', 0, 2012, 'ITALIA', 190, 'comici', 'Elizabeth Banks, Steven Brill, Steve Carr, Rusty C', 'Un cast stellare per questa folle e dissacrante comedy a episodi nata dalla mente dei fratelli Farrelly (Tutti pazzi per Mary).', 'Emma Stone, Elizabeth Banks', 12),
(22, 'Il cacciatore di giganti ', 'il_cacciatore_di_giganti.jpg', '26.00', 0, 2010, 'USA', 170, 'azione', 'Bryan Singer ', 'Una bellicosa razza di giganti dichiara guerra agli umani per riconquistare i territori un tempo perduti. Jack, un giovane ragazzo, lottera`  per fermarli e per salvare una principessa rapita.', 'Ewan McGregor, Nicholas Hoult', 56),
(23, 'I Croods ', 'i_croods.jpg', '10.00', 0, 2000, 'FRANCIA', 180, 'animazione', 'Kirk De Micco e Chris Sanders ', 'Una famiglia di cavernicoli in viaggio per cercare una nuova casa, incontrera`  lungo la strada un giovane ragazzo il cui pensiero ribelle e anticonformista si scontrera`  con quello fortemente tradizionale del capofamiglia.', 'Nicolas Cage, Ryan Reynolds, Emma Stone ', 59),
(21, 'Captive ', 'captive.jpg', '25.00', 0, 2011, 'ITALIA', 160, 'avventura', 'Brillante Mendoza ', 'L`odissea di alcuni turisti stranieri che nelle Filippine vengono rapiti da un gruppo di separatisti islamici (Abu Sayyaf),', 'Isabelle Huppert, Kathy Mulville', 53),
(20, 'Cloud Atlas ', 'cloud_atlas.jpg', '12.00', 0, 2012, 'FRANCIA', 150, 'avventura', 'Tom Tykwer, Andy Wachowski, Lana Wachowski ', 'Il film racconta una serie di storie interconnesse tra di loro che tra passato, presente e futuro influenzeranno le vite e i destini dei diversi protagonisti.', 'Tom Hanks, Hugo Weaving', 50),
(19, 'The Conjuring ', 'the_conjuring.jpg', '15.00', 0, 2011, 'USA', 160, 'thriller', 'James Wan ', 'Ispirato ad eventi reali il film racconta di come Ed e Lorraine Warren, investigatoiri esperti in sovrannnaturale, aiuteranno una famiglia a liberare la loro casa da una presenza maligna.', 'Vera Farmiga, Patrick Wilson', 47),
(18, 'Cattivissimo me 2 ', 'cattivissimo_me_2.jpg', '20.90', 0, 2012, 'ITALIA', 152, 'animazione', 'Pierre Coffin e Chris Renaud ', 'Tornano in una nuova avventura ricca di azione, risate e gadget il cattivo dal cuore d`oro Gru, le sue tre bambine e gli spassosi minion.', 'Al Pacino, Jason Segel, Steve Carell ', 44),
(17, 'Carrie ', 'carrie.jpg', '46.00', 0, 2000, 'FRANCIA', 144, 'horror', 'Kimberly Peirce ', 'Remake del classico anni ''70 di Brian De Palma basato su un romanzo di Stephen King. Carrie è una ragazza timida e impacciata che viene emarginata e vessata dai suoi compagni di scuola e tormentata dalla madre oppressiva, ma la scoperta di possedere poteri telecinetici scatenera`  una sanguinosa vendetta.', 'Chloe Moretz, Julianne Moore, Alex Russell', 41),
(16, 'Cercasi amore per la fine del mondo ', 'cercasi_amore_per_la_fine_del_mondo.jpg', '25.00', 0, 2010, 'USA', 136, 'commedia', 'Lorene Scafaria ', 'Dodge e Penny intraprendono un ultimo viaggio in macchina mentre la Terra sta per essere colpita da un asteroide che cancellera`  l`umanita` ', 'Steve Carell, Keira Knightley', 38),
(15, 'La casa - Evil Dead ', 'la_casa_-_evil_dead.jpg', '12.00', 0, 2010, 'ITALIA', 128, 'horror', 'Fede Alvarez ', 'Remake del classico horror anni â€˜80 di Sam Raimi qui in veste di produttore. Un gruppo di ragazzi si reca in un cottage isolato nei boschi dove risveglia un`antica e inarrestabile forza demoniaca.', 'Jane Levy, Jessica Lucas', 35),
(14, 'Buon Anno Sarajevo ', 'buon_anno_sarajevo.jpg', '35.00', 0, 2007, 'USA', 120, 'avventura', 'Aida Begic ', 'Rahima e Nedim sono due fratelli che vivono a Sarajevo, un incidente a scuola che coinvolgera`  Nadim portera`  Rahima a scoprire che il fratello conduce una doppia vita.', 'Marija Pikic, Ismir Gagula ', 32),
(13, 'Burt Wonderstone ', 'burt_wonderstone.jpg', '10.00', 0, 2012, 'ITALIA', 112, 'commedia', 'Don Scarno ', 'Due illusionisti di Las Vegas a seguito di un incidente si separano, ma torneranno a far squadra quando un terzo mago carismatico e alla moda comincera`  a metterli in ombra.', 'Steve Carell, Jim Carrey, Olivia Wilde', 29),
(12, 'Broken City ', 'broken_city.jpg', '16.00', 0, 2012, 'FRANCIA', 178, 'thriller', 'Allen Hughes con Mark Wahlberg, Russell Crowe, Cat', 'Un ex-poliziotto ora detective privato indaga per conto di una potente figura politica sull`amante della moglie di quest`ultimo, ne seguira`  un omicidio che inneschera`  indagini nell`ambiguo mondo della politica.', '', 26),
(11, 'The Big Wedding ', 'the_big_wedding.jpg', '25.00', 0, 2006, 'USA', 170, 'commedia', 'Justin Zackham ', 'Don e Ellie Griffin, una coppia da tempo divorziata e in perenne conflitto, saranno costretti ad interpretare la parte di una coppia felice per il bene del matrimonio del loro figlio adottivo,', 'Robert De Niro, Diane Keaton', 45),
(10, 'Beautiful Creatures - La sedicesima luna di Richard LaGravenese-', 'beautiful_creatures_-_la_sedicesima_luna_di_richard_lagravenese-.jpg', '25.90', 0, 2005, 'ITALIA', 162, 'thriller', 'Richard LaGravenese', 'Ethan e Lena scoprono di avere una connessione sovrannaturale legata al passato della citta`  in cui vivono e delle loro famiglie che nascondono segreti inimmaginabili.', '', 43),
(9, 'Battle of the Year La vittoria e` in ballo di Benson Lee ', 'battle_of_the_year_la_vittoria_e_in_ballo_di_benson_lee.jpg', '12.00', 0, 2009, 'ITALIA', 154, 'commedia', 'Antoine Fuqua', 'Una crew di ballerini e il loro allenatore dopo molte delusioni affrontano il pia`¹ grande campionato di break-dance.', '', 41),
(8, 'After Earth - Dopo la fine del mondo ', 'after_earth_-_dopo_la_fine_del_mondo.jpg', '5.90', 0, 2004, 'USA', 146, 'fantascienza', 'M. Night Shyamalan ', 'Mille anni dopo che eventi catastrofici hanno costretto l`umanita`  ad abbandonare la Terra, Cypher Raige e Kitai, padre e figlio, a bordo di una navicella precipitano su un pianeta divenuto ostile e pericoloso. Con il padre ferito tocchera`  a Kitai partire in cerca di aiuto.', 'Will Smith, Jaden Smith, Isabelle Fuhrman ', 39),
(7, 'Gli amanti passeggeri ', 'gli_amanti_passeggeri.jpg', '30.90', 0, 2003, 'ITALIA', 138, 'commedia', 'Pedro Almodovar ', 'Un gruppo di viaggiatori si ritrova alle rpese con situazione di pericolo a bordo di un aereo diretto a Citta`  del Messico, ognuno l`affrontera`  a modo proprio.', 'Penelope Cruz, Antonio Banderas', 37),
(5, 'Attacco al potere - Olympus Has Fallen ', 'attacco_al_potere_-_olympus_has_fallen.jpg', '25.00', 0, 2012, 'USA', 122, 'azione', 'Antoine Fuqua ', 'Un ex-agente dei Servizi segreti si trasforma nell`ultima speranza della nazione quando la Casa Bianca viene espugnata da alcuni terroristi.', 'Gerard Butler, Aaron Eckhart, Melissa Leo', 33),
(6, 'Le avventure di Fiocco di neve ', 'le_avventure_di_fiocco_di_neve.jpg', '29.90', 0, 2002, 'FRANCIA', 130, 'commedia', 'Andras G. Schaer ', 'Un cucciolo rarissimo di gorilla bianco approda allo zoo di Barcellona e diventa una grande attrazione, ma i suoi simili non lo accettano e lo isolano.', 'Elsa Pataky, Pere Ponce, Claudia Abate', 35),
(4, 'Asterix e Obelix al servizio di sua maesta`', 'asterix_e_obelix_al_servizio_di_sua_maesta.jpg', '9.90', 0, 2001, 'ITALIA', 184, 'commedia', 'Laurent Tirard ', 'Trasferta britannica per la celebre coppia di Galli che stavolta accorre in soccorso della Regina Cordelia minacciata dalla megalomania del solito Cesare.', 'Gerard Depardieu, Edouard Baer', 31),
(155, 'A Royal Weekend', 'a_royal_weekend.jpg', '10.08', 0, 2010, 'Francia', 172, 'Thriller', 'Roger Michell', 'Giugno 1939 il Presidente Franklin Delano Roosevelt e sua moglie Eleanor ospitano il re e la regina di Inghilterra per un weekend nella loro casa di Hyde Park on Hudson.', 'Bill Murray, Laura Linney, Olivia Williams, Olivia Colman', 29),
(1, 'Anna Karenina ', 'anna_karenina.jpg', '25.90', 0, 2000, 'ITALIA', 148, 'avventura', 'Joe Wright ', 'Adattametnto della celebre opera di Tolstoj dal regista di Orgoglio e pregiudizio.', 'Keira Knightley, Jude Law ', 25),
(2, 'Anime nella nebbia - V Tumane ', 'anime_nella_nebbia_-_v_tumane.jpg', '26.50', 0, 2011, 'USA', 160, 'thriller', 'Sergei Loznitsa ', 'Seconda guerra mondiale, in una foresta due partigiani si confrontano, uno dei due è accusato di aver collaborato con il nemico.', 'Vladimir Svirski, Vlad Abashin', 27),
(46, 'Hunger Games La ragazza di fuoco ', 'hunger_games_la_ragazza_di_fuoco.jpg', '20.50', 0, 2012, 'USA', 80, 'azione', 'Francis Lawrence con Jennifer Lawrence, Josh Hutch', 'Secondo capitolo della trilogia basata sui romanzi di Suzanne Collins. Katniss torna a casa con l`amico Peeta dopo aver vinto la 74Âª edizione degli Hunger Games. Impegnata in un tour nei vari distretti tocchera`  con mano la ribellione che sta montando di cui lei è il cuore pulsante. ma a Capitol city il Presidente Snow trama per mantenere il controllo e organizza un nuovo speciale torneo.', '', 40),
(47, 'Hansel & Gretel - Cacciatori di streghe ', 'hansel_&_gretel_-_cacciatori_di_streghe.jpg', '49.00', 0, 2011, 'FRANCIA', 90, 'commedia', 'Tommy Wirkola ', 'I fratelli Hansel e Gretel dopo le vicende narrate nella celebre fiaba dei fratelli Grimm sono ormai adulti e hanno messo a frutto la loro traumatica esperienza per diventare degli abili ammazzastreghe e non solo.', 'Jeremy Renner, Gemma Arterton ', 42),
(48, 'The Heat ', 'the_heat.jpg', '25.00', 0, 2010, 'ITALIA', 100, 'commedia', 'Paul Feig ', 'Un`agente dell`FBI arrogante e scrupolosa si trova costretta a far squadra con una sboccata poliziotta di Boston dai metodi ben poco convenzionali per riuscire ad incastrare un grosso trafficante di droga,', 'Sandra Bullock, Melissa McCarthy, Kaitlin Olson, Michael Rapaport ', 44),
(49, 'The Host ', 'the_host.jpg', '46.00', 0, 2001, 'USA', 110, 'fantascienza', 'Andrew Niccol ', 'Basato sull`omonimo romanzo di Stephenie Meyer (Twilight). Dopo una devastante invasione aliena da parte di esseri incorporei denominati Anime, alcune frange di resistenti cercano di sopravvivere alll`occupazione, tra loro c`è Melanie Stryder che pera`² viene catturata e la sua mente posseduta da un alieno noto come Wanderer.', 'Saoirse Ronan, Diane Kruger, Max Irons, Jake Abel, William Hurt ', 46),
(50, 'Amanti perduti', 'amanti_perduti.jpg', '19.90', 0, 2002, 'ITALIA', 180, 'classici', 'Marcel Carne', 'Una compagnia d''artisti ottiene grande successo in un teatrino popolare della Parigi ottocentesca. Vi agisce, tra gli altri, un mimo, Battista, artista dal temperamento romantico e sentimentale, che s''innamora romanticamente di una ragazza belloccia ed equivoca, Garance. Questa l''ama a modo suo e sarebbe disposta a divenirne l''amante; ma non comprende l''amore spirituale del mimo..', 'Arletti,JeanLouis Barrault', 25),
(51, 'Amore e guerra', 'amore_e_guerra.jpg', '29.90', 0, 2003, 'USA', 78, 'classici', 'Woody Allen', 'Il russo Boris (Allen) è follemente innamorato della bella Sonja (Keaton) e terrorizzato dalle guerre napoleoniche. Sconvolto dalla notizia delle nozze di Sonja, si arruola come soldato per tornare in patria come eroe. Finalmente convintasi a sposare lui, Sonja offre al povero Boris una vita ricca di filosofia, astinenza e per cibo...neve! Qando i francesi invadono laRussia...', 'Woody Allen, Diane Keaton', 25),
(52, 'Aguirre furore di Dio', 'aguirre_furore_di_dio.jpg', '20.90', 0, 2001, 'SPAGNA', 123, 'classici', 'Werner Herzog', 'Nel 1560 una spedizione spagnola guidata da Gonzalo Pizarro, discende la cordigliera delle Ande alla ricerca del mitico El Dorado. La giungla inestricabile la blocca. Si invia allora una pattuglia esplorativa, munita di zattere, sul fiume Urubamba al comando di Pedro de Urrua, al cui fianco è l''ambizioso e spietato Lope de Aguirre. Finirà vittima della sua folle megalomania.', 'Klaus Kinski, Helena Rojo', 50),
(53, 'Alien', 'alien.jpg', '19.90', 0, 2000, 'ITALIA', 121, 'classici', 'Ridley Scott', 'Nello spazio nessuno può sentirti urlare! L''equipaggio di una nave spaziale trova su un pianeta sconosciuto uno strano essere a forma di granchio. Lo portano a bordo e l''alieno comincia a menare strage fra gli astronauti. Il comandante chiede aiuto alla base ma scopre che laggiù sono molto più interessati all''alieno che alla sorte dei terrestri...', 'Sigourney Weaver, John Hurt, Harry Stanton', 25),
(54, 'Always- Per sempre', 'always-per_sempre.jpg', '29.90', 0, 2008, 'FRANCIA', 118, 'classici', 'Steven Spielberg', 'Dorinda perde l''uomo della sua vita in una pericolosa missione di soccorso aereo, ma il pilota resta sotto forma di spirito per rimanerle vicino, restituirle la felicità e renderla nuovamente capace di amare. Una toccante fiaba romantica, in cui l''amore vince sulla morte, diretta da S.Spielberg e interpretata da un cast d''eccezione, tra cui A.Hepburn nella sua ultima interpretazione.', 'Holly Hunter,Audrey Hepburn', 50),
(55, 'Casablanca', 'casablanca.jpg', '25.90', 0, 2007, 'ITALIA', 112, 'classici', 'Michael Curtiz', 'Il triangolo amoroso e il locale più famoso della storia del cinema. Durante la 2° Guerra Mondiale, al Rick''s bar di Casablanca gestito da Rick Blaine compaiono Victor Laszlo, eroe della Resistenza, la moglie Ilse, ex fiamma mai spenta di Rick, in cerca di due "lettere di transito" per lasciare il Marocco. Ci penserà Rick a procurarle, gettando via quel velo di cinismo che sembrava aver fatto sparire ogni spirito patriottico.', 'Claude Rains, Paul Henreid', 40),
(56, 'Arancia Meccanica', 'arancia_meccanica.jpg', '19.90', 0, 2007, 'USA', 111, 'classici', 'Stanley Kubrick', 'Alex (Malcolm McDowell) è la violenza allo stato puro. Teppista minorenne in una Londra di un futuro non troppo lontano, ruba, uccide, incendia senza paure, senza esitazioni nè rimorsi. Ma un giorno la polizia lo cattura e lo mette nelle mani di uno scienziato che lo sottopone a un trattamento speciale dove...', 'Patrick Magee, Warren Clarke', 26),
(57, 'Barry Lindon', 'barry_lindon.jpg', '29.90', 0, 2005, 'USA', 101, 'classici', 'Stanley Kubrick', 'Redmond Barry, un giovane irlandese di modeste condizioni, sfida a duello, per amore di sua cugina Nora, un capitano inglese che l''ha chiesta in moglie. Convinto d''aver ucciso il rivale, parte per Dublino, ma lungo la strada viene derubato, per cui è costretto ad arruolarsi nell''esercito inglese, in lotta contro i francesi. La morte di un capitano suo amico, e le atrocità della guerra, lo inducono a disertare, ma è scoperto e per cavarsela non gli resta che militare con i prussiani, alleati degli inglesi. Essendosi fatto onore sul campo di battaglia, si vede affidare, dal ministro della polizia prussiana, l''incarico di sorvegliare uno straniero, giocatore professionista, che si suppone sia una spia. Diventato suo amico, invece, Barry lascia la Prussia per seguirlo nei suoi pellegrinaggi fra la buona società'' di mezza Europa. Gli capita, così, d''incontrare la bella Lady Lyndon, moglie di un uomo anziano e malandato, alla cui morte Barry succede nel talamo. Dovrebbe, questo matrimonio, essere il culmine della sua fortuna: lo condurrà, invece, alla rovina.', 'Marisa Berenson', 23),
(58, 'C`era una volta in america', 'c`era_una_volta_in_america.jpg', '16.90', 0, 2006, 'ITALIA', 180, 'classici', 'Sergio Leone', 'La storia di Max Bercovicz (James Woods) Max, e David Aaronson (Robert De Niro) chiamato Noodles, comincia negli anni ''20 quando i due, ancora ragazzini, iniziano la loro carriera nella malavita del quartiere ebraico di New Jork. Dopo alterne vicende e drammatici eventi all''interno della criminalità organizzata, Max finirà come politico corrotto e bancarottiere, Noodles come vecchio solitario e deluso dalla vita...', 'Danny Aiello, Robert De Niro', 24),
(59, 'Dracula', 'dracula.jpg', '9.90', 0, 2004, 'USA', 125, 'classici', 'Tod Browning', 'Il Conte Dracula (Bela Lugosi) si muove dai Carpazi sino in Inghilterra per seguire la bella Mina (Helen Chandler), moglie di John Harker, venuto sino al suo Castello per affari. Ad essere morse saranno sia Mina che Lucy Weston, prima che il dottor Van Helsing (Edward Van Sloan) possa diagnosticare il mutamento del loro sangue..', 'Bela Lugosi, helen Chandler', 22),
(60, 'Cordura', 'cordura.jpg', '16.90', 0, 2003, 'USA', 135, 'classici', 'Robert Rossen', 'Durante la guerra contro Pancho Villa, l''ufficiale Thomas Thorn ha il compito di condurre cinque candidati alla medaglia d''onore fino alla base militare di Cordura, nel Texas. Con loro c''è anche un''espatriata americana accusata di fornire sostegno ai messicani. Sarà un cammino pieno di pericoli che svelerà un coraggio molto più profondo dell''effimero eroismo della battaglia.', 'Gary Cooper, Rita Hayworth', 20),
(61, 'Day of Wrath', 'day_of_wrath.jpg', '19.90', 0, 2001, 'DENMARK', 144, 'classici', 'Carl Dreyer', 'Danimarca: corre l''anno 1632 e tutto il paese è avvolto nella cupa atmosfera della Riforma luterana. Il giovane Martino, terminati gli studi, ritorna alla casa paterna. Martino è figlio di primo letto del giudice e pastore Assalonne Pederson che, rimasto vedovo, ha sposato la giovane Anna. Martino fa la sua conoscenza della matrigna, che ha spostato suo padre non per amore ma per riconoscenza: il pastore ha infatti salvato dal rogo sua madre, accusata di stragoneria. Tra la matrigna e il figliastro sorge un amore improvviso...', 'Sigurd Berg', 15),
(62, 'Andrej Rubliov', 'andrej_rubliov.jpg', '20.90', 0, 2000, 'RUSSIA', 185, 'classici', 'Andreij Tarkovskij', 'Andreij è un pittore di icone che nella Russia devastata dalle invasioni barbariche attraversa una crisi di identità...', 'Anatoly Solomitsyn, Ivan Lapikov', 15),
(63, 'Aladin', 'aladin.jpg', '16.90', 0, 2001, 'SPAGNA', 124, 'animazione', 'John Musker', 'C''era una volta il mitico regno di Agrabah, un paese orientale dominato da un bonario Sultano. E c''era un teppistello chiamato Aladdin, il quale sognava di diventare ricco e di sposare la figlia del Sultano, Jasmine. Tutto comiciò quando l''ambiguo Visir, Jaffar, incaricò il giovane di recuperare una certa lampada. ', 'Alan Kicking', 20),
(64, 'Alpha and Omega', 'alpha_and_omega.jpg', '17.90', 0, 2002, 'ITALIA', 121, 'animazione', 'Dennis Hopper', 'Kate è una giovane lupachiotta, forte e determinata, Humpherey invece un giovane lupo spensierato e che vive alla giornata. I due non potrebbero essere più diversi e infatti si trovano spesso in contrasto, fino al giorno in cui vengono catturati dai ranger del parco e portati lontano dal loro habitat.  ', 'Hayden Panettiere', 20),
(65, 'Alla ricerca di Nemo', 'alla_ricerca_di_nemo.jpg', '19.90', 0, 2003, 'AUSTRALIA', 134, 'animazione', 'Andrew Stanton', 'Il piccolo Nemo, pesce pagliaccio curioso e intraprendente, viene catturato da un sub mentre il padre Marlin assiste impotente alla scena. Comincia così un viaggio tra mille pericoli per riuscire a ritrovare Nemo finito in un acquario di un dentista di Sidney.', 'Alexander Gould', 25),
(66, 'Asterix e i vichingi', 'asterix_e_i_vichinghi.jpg', '29.90', 0, 2007, 'DENMARK', 155, 'animazione', 'Jesper Moller', 'Nel piccolo villaggio della Gallia arriva da Lutezia il giovane Goudurix, nipote del capovillaggio, che viene affidato alle cure di Asterix ed Obelix, incaricati di fornirgli gli insegnamenti necessari a farlo diventare un uomo saggio e valoroso. Dietro l''aspetto da adolescente arrogante e sbruffone, Goudurix però nasconde un animo pauroso...', 'Stefan Fjeldmark', 20),
(67, 'Astro Boy', 'astro_boy.jpg', '28.90', 0, 2008, 'SPAGNA', 176, 'animazione', 'David Bowers', 'Astro Boy è la storia di un giovane robot con poteri incredibili, creato da un brillante scienziato ad immagine del figlio che ha perso. Incapace di soddisfare le aspettative dell''uomo, il nostro eroe si imbarca in un viaggio per ritrovare se stesso..', 'Mark Handler', 15),
(68, 'Azur e Asmar', 'azur_e_asmar.jpg', '20.90', 0, 2010, 'FRANCIA', 111, 'animazione', 'Michel Ocelot', 'Il biondo Azur, dagli occhi celesti, e Asmar, dagli occhi e i capelli neri, crescono insieme in Francia. Da piccoli si battono e si amano come due fratelli. Separati per alcuni anni, i due amici d''infanzia, uno cristiano e l''altro musulmano si ritroveranno come rivali alla ricerca della fata dei Ginn...', 'Lucky Red', 50),
(69, 'Bee Movie', 'bee_movie.jpg', '19.90', 0, 2011, 'ITALIA', 109, 'animazione', 'Steve Hicker', 'Barry Bee Benson, giovane ape neolaureata, non vuole rassegnarsi al suo triste destino: produrre miele per tutta la vita. Tuttavia, dopo l''incontro con Vanessa, una fioraia di New York che gli salva la vita, Barry diventa consapevole di quanto il miele si apprezzato dagli esseri umani e per questo decide di accettare l''incarico che Madre Natura gli haassegnato.', 'Simon Smith', 75),
(70, 'Bolt-Un eroe a quattro zampe', 'bolt-un_eroe_a_quattro_zampe.jpg', '15.90', 0, 2012, 'USA', 104, 'animazione', 'Chris Williams', 'Il cagnolino Bolt, protagonista di uno show televisivo di successo, è cresciuto tra riflettori e telecamere ed è convinto di essere dotato di superpoteri. Un giorno, Bolt viene accidentalmente allontanato dal set e, disperato, realizza che la vita reale è ben diversa dalla finzione sullo schermo. Ad aiutarlo nel suo viaggio attraverso l''America per ritrovare la strada di casa sarà l''amicizia con Mr. Mittens, una gatta domestica abbandonata, e con Rhino, un criceto rinchiuso in una palla di plastica e ossessionato dalla TV.', 'Mark Ceethor', 50),
(71, 'Act of valor', 'act_of_valor.jpg', '9.90', 0, 2000, 'USA', 120, 'avventura', 'Mike McCoy', 'Una missione per liberare un agente della CIA rapito svela per caso una trama agghiacciante, con conseguenze potenzialmente inimmaginabili, e una squadra scelta tra le élite delle forze armate, un gruppo di guerrieri ad altissimo addestramento, viene inviata in missione top-secret. I valorosi uomini del Plotone "Bandito" cominciano una corsa contro il tempo da un punto caldo del mondo all''altro in una missione che si allargherà sempre di più. ', 'Roselin Sanchez', 25),
(72, 'A-team', 'a-team.jpg', '19.90', 0, 2001, 'USA', 121, 'avventura', 'Joe Carnahan', 'Versione cinematografica del celebre telefilm creato da Stephen J. Cannell, che vede protagonista una squadra di ex combattenti che diventano mercenari al servizio delle buone cause.', 'Jessica Biel, Liam Neeson', 25),
(73, 'After the sunset', 'after_the_sunset.jpg', '29.90', 0, 2003, 'USA', 121, 'avventura', 'Brett Ratner', 'Max Burdett (Pierce Brosnan) e Lola Cirillo (Salma Hayek), due maestri del furto, fuggono in un paradiso tropicale per andarsi a godere il frutto della loro ultimo colpo. Un agente dell''FBI sulle loro tracce da sette anni, è però convinto che i due stiano architettando un piano per rubare un anello da un milione di dollari...', 'Pierce Brosnan, Salma Hayek', 25),
(74, 'Alieni in soffitta', 'alieni_in_soffitta.jpg', '19.90', 0, 2012, 'USA', 131, 'avventura', 'John Schultz', 'Un gruppo di ragazzini si trova costretto a combattere contro gli alieni, arrivati sulla Terra per prenderne il controllo.', 'Doris Roberts, Robert Hoffman', 50),
(75, 'Apocalypto', 'apocalypto.jpg', '19.90', 0, 2011, 'USA', 135, 'avventura', 'Mel Gibson', 'L''impero dei Maya è in declino e un popolo invasore sta distruggendo i fasti dell''antica civiltà. Gli alti esponenti del potere, per ingraziarsi gli dei, continuano a pretendere la costruzione di nuovi templi e ulteriori sacrifici umani. Tra i prescelti al sacrificio c''è Jaguar Paw che però è deciso a sfuggire al proprio destino. Si avventura nell''ardua impresa di salvare se stesso e il suo popolo ma, soprattutto, la sua famiglia e la donna che ama.', 'Rudy Youngblood', 50),
(76, 'Balla coi lupi', 'balla_coi_lupi.jpg', '19.90', 0, 2010, 'USA', 156, 'avventura', 'Kevin Costner', 'Nel 1863, durante la Guerra di Secessione, il tenente John J. Dumbar (Kevin Costner), eroe suo malgrado, decide di aggregarsi a una tribù di Sioux per conoscere "la frontiera" prima che scompaia... Tratto dal romanzo omonimo di Michael Blake.', 'Kevin Costner, Graham Greene', 50),
(77, 'Big city', 'big_city.jpg', '9.90', 0, 2010, 'FRANCIA', 111, 'avventura', 'Djamel Bensalah', 'Quando gli adulti del villaggio corrono in soccorso di una carovana che è stata attaccata dagli Indiani, i bambini al risveglio, il giorno dopo, si ritrovano improvvisamente da soli...Hanno una città a disposizione tutta per loro, e senza il controllo dei genitori...Così i piccoli di "Big City" cominciano a vivere come gli adulti, e ognuno, esattamente come i propri genitori, si occupa di qualcosa all''interno della città...Tutto sembra filare liscio fino a quando "Big City" non viene attaccata dai figli degli Indiani...', 'Vincent Valladon', 50),
(78, 'A testa alta', 'a_testa_alta.jpg', '15.90', 0, 2008, 'Afghanistan', 109, 'Animazione', 'Kevin Bray', 'Dopo essersi ritirato dalle forze speciali dell&#039;esercito, Chris Vaughn decide di tornare nel suo piccolo paese di infanzia e ricominciare una nuova vita. Qui perï¿½ le cose sono cambiate: la droga, la corruzione e la criminalitï¿½ dilagano e il responsabile di tutto ciï¿½ ï¿½ un vecchio rivale di Chris...', 'Dwayne Johnson, Kristen Wilson', 50),
(79, 'Adrenalina blu', 'adrenalina_blu.jpg', '29.90', 0, 2007, 'USA', 105, 'azione', 'Louis Couvelaire', 'Michel Vaillant, eccellente pilota, è il campione incontestato dei rally e dei circuiti di tutto il mondo. I suoi successi e quelli della sua squadra, la scuderia Vaillante, suscitano ammirazione, ma anche gelosia fra gli altri concorrenti... Dal fumetto culto degli anni ''60, un film spettacolare scritto da Luc Besson.', 'Diane Kruger, S. Stevenin', 50),
(80, 'Air force one', 'air_force_one.jpg', '19.90', 0, 2005, 'USA', 99, 'azione', 'Wolfgang Petersen', 'A bordo dell''aereo presidenziale Air Force One, il presidente degli USA (Harrison Ford) di ritorno a casa con la famiglia subisce il ditorramento da parte dei terroristi...', 'Harrison Ford, Glenn Close', 75),
(81, 'Armageddon', 'armageddon.jpg', '16.90', 0, 2003, 'USA', 76, 'azione', 'Michael Bay', 'Qaundo il direttore esecutivo della NASA Dan Truman apprende che la terra ha solamente a disposizione 18 giorni prima che venga completamente distrutta da una meteora grande quanto il Texas, ha solo una possibilità per la salvezza del pianeta: far atterrare sull''asteroide il migliore team di trivellatori petroliferi e permettere loro di installare le testate nucleari che annienteranno l''obiettivo.', 'Bruce Willis, Liv Tyler', 75),
(82, 'Beverly Hills Cop 2', 'beverly_hills_cop2.jpg', '14.90', 0, 2001, 'USA', 78, 'azione', 'Tony Scott', 'Nel secondo episodio Axel Foley torna a Beverly Hills per andare a trovare il Capitano Andrew Bogomil (Ronny Cox) che è stato gravemente ferito da un colpo di pistola. Qui si riunisce con i colleghi Billy Rosewood (Judge Reinhold) e John Taggart (John Ashton) e decidono di trovare il colpevole, una donna bionda e altissima (Brigitte Nielsen). Contro di loro il Capo della Polizia Harold Lutz che vuole licenziarli.', 'Eddie Murphy, John Ashton', 75),
(83, 'I soliti idioti', 'i_soliti_idioti.jpg', '19.90', 0, 2009, 'USA', 101, 'comici', 'Enrico Lando', 'Ruggero De Ceglie e suo figlio Gianluca, intraprendono un lungo viaggio attraverso l''Italia. Ruggero, padre autoritario, volgare e disonesto, trascinerà Gianluca, ragazzo dall''animo sensibile, amante dell''arte e della tecnologia, in situazioni rocambolesche per far assaporare al figlio la vera vita e farlo crescere a modo suo. ', 'F.Biggio, M.Ghenea', 50),
(84, 'Johnny English', 'jonny_english.jpg', '19.90', 0, 2007, 'FRANCIA', 108, 'comici', 'Oliver Parker', 'Nella sua nuova avventura, Johnny English - l''agente segreto che non conosce pericolo e paura - deve fermare un gruppo di killer internazionali prima che facciano fuori il premier cinese, gettando il mondo nel caos. Johnny, che si era ritirato in una remota regione asiatica a perfezionare le sue abilità, viene reclutato dall''agenzia perchè affronti i terroristi. Dovrà necessariamente aggiornarsi dal punto di vista tecnologico, ma il tempo è poco e Johnny deve far ricorso a qualsiasi asso nella manica per salvare il mondo. E se per lui il disastro può essere un''opzione, il fallimento non lo è.  ', 'R.Atkinson, G.Anderson', 50),
(85, 'Matrimonio a Parigi', 'matrimonio_a_parigi.jpg', '9.90', 0, 2010, 'USA', 107, 'comici', 'Claudio Risi', 'Durante una vacanza a Parigi, la figlia di un industriale abituato ad evadere le tasse con ogni mezzo, si innamora ricambiata del figlio di un onesto finanziere. Le due famiglie si ritroveranno a condividere la stessa camera d''albergo e gli scontri (ma anche i cambiamenti) diventeranno inevitabili. ', 'M.Boldi, B.Izzo', 25),
(86, 'Prima Pagina', 'prima_pagina.jpg', '9.90', 0, 2009, 'GERMANIA', 111, 'comici', 'Billy Wilder', 'Siamo a Chicago nel 1929. I giornalisti dei principali quotidiani soggiornano nella sala stampa della Corte Criminale in attesa dell''esecuzione di Earl Williams, condannato all''impiccagione per la presunta uccisione di un poliziotto di colore e, in realtà, per il fatto di appartenere alla "Friends of American Liberty", organizzazione a cavallo tra l''anarchico e il marxista. Tra i giornalisti eccelle Hildy Johnson dell''Examiner il quale, tuttavia, declina l''incarico poichè è deciso a sposare Peggy Grant, a trasferirsi immediatamente a Philadelphia e a dedicarsi alla pubblicità. Il suo direttore, l''astuto e cinico Walter Burns, visti inutili i tentativi di fermare il suo brillante dipendente, cerca di stuzzicarne l''orgoglio sostituendolo con uno sprovveduto pivello...', 'S.Adler, W.Matthau', 25),
(87, 'Succhiami', 'succhiami.jpg', '29.90', 0, 2012, 'SPAGNA', 121, 'comici', 'Danny Trejo', 'Dopo 3 film e 5 anni di bacetti al chiaro di luna, Edward e Bella si danno alla pazza gioia spaccando spalliere dei letti in luna di miele! Jacob smette di gridare "Al Lupo, Al Lupo"e amareggiato e depresso abbandona anni di palestra per buttarsi sui carboidrati. Tutto sembra finito, ma dei valori del sangue sballati regaleranno nuove sorprese al triangolo amoroso più tormentato della storia. ', 'N.Kohlmann', 25),
(88, 'Vacanze di Natale a Cortina', 'vacanze_di_natale_a_cortina.jpg', '19.90', 0, 2012, 'USA', 122, 'comici', 'N.Parenti', 'Il tradizionale cinepanettone 2011 è ambientato a Cortina d''Ampezzo, e interpretato ancora una volta da Christian De Sica. Una storia che prende di mira vizi e virtù degli italiani in una satira pungente ambientata tra le nevi della celebre località località turistica invernale. ', 'C.De Sica, S.Ferilli', 25),
(89, 'Una notte sui tetti', 'una_notte_sui_tetti.jpg', '14.90', 0, 2011, 'USA', 133, 'comici', 'H.Marx', 'Un detective indaga sul furto di una preziosa collana risalente al periodo degli zar di Russia, e si scopre che a farla sparire è stato un attore muto che fa parte di una compagnia di attori squattrinati e desiderosi di andare in scena a Broadway. ', 'G.Marx, C.Marx', 25),
(90, 'I 2 soliti idioti', 'i_2_soliti_idioti.jpg', '19.90', 0, 2011, 'USA', 144, 'comici', 'Enrico Lando', 'In questo sequel, ritroviamo Ruggero e il figlio Gianluca in fuga dalla gang dei Russi. Ma in questo anno tante cose sono cambiate e la crisi si è abbattuta anche sull''impero del Wurstel. Per salvarlo, Ruggero è disposto a tutto, anche a ricorrere all''aiuto del padre di Fabiana, un uomo rigoroso e austero. Amnesie, imbrogli e sotterfugi saranno solo alcuni degli ingredienti di quella che si preannuncia come una rocambolesca epopea: riuscirà Ruggero a salvare il suo impero, scappare dai Russi e soprattutto salvarsi dalla temibile scure dell''erario? Accanto ai protagonisti principali della storia, appaiono una lunga serie di altri strani personaggi: i ragazzi coatti milanesi, Patrick e Alexio, già diventati il nuovo tormentone, i bambini di "Mamma esco", Niccolo e Gigetto, i preti marketing e altri totalmente nuovi, creati appositamente per il film. ', 'F.Mandelli, F.Biggio', 50),
(91, 'Frankenstein Junior', 'frankenstein_junior.jpg', '9.90', 0, 2012, 'ITALIA', 120, 'comici', 'Mel Brooks', 'In questo capolavoro della comicità contemporanea, il protagonista non è più il barone Victor, ma il nipote dottor Frederick Frankenstein (Gene Wilder), che viene convocato suo malgrado per il testamento nel castello del nonno in Transilvania. Qui Frederick scopre il fatidico manuale di istruzioni per ridare la vita ai morti e, con l''aiuto del gobbo (?) Igor (Marty Feldman) e della sensuale e sempre eccitata Inga (Teri Garr), crea un mostro che, in realtà, non è cattivo e vuole solo essere amato...', 'M.Kahn, C.Leachman', 75),
(92, 'Che bella giornata', 'che_bella_giornata.jpg', '19.90', 0, 2001, 'USA', 110, 'comici', 'Gennaro Nunziante', 'Checco, security di una discoteca della Brianza, sogna di fare il carabiniere ma viene respinto al colloquio. Grazie alla raccomandazione di uno zio presso il vescovo di Milano, si ritrova a lavorare come addetto alla sicurezza del Duomo. Qui conosce Farah, una ragazza araba che si finge studentessa di architettura per avvicinare la Madonnina, ai piedi della quale medita in realtà di depositare una bomba per vendicare l''uccisione della sua famiglia. ', 'Checco Zalone, Tullio Solenghi', 75),
(93, 'Natale a Miami', 'natale_a_miami.jpg', '29.90', 0, 2000, 'ITALIA', 100, 'comici', 'Neri Parenti', 'Siamo alla vigilia delle vacanze natalizie. Ranuccio (Massimo Boldi), Giorgio (Christian De Sica) e Paolo (Francesco Mandelli) hanno in comune un destino sfortunato. Tutti e tre vengono lasciati dalle rispettive mogli e compagne. Tutti e tre cadono nella stessa terribile depressione. Giorgio si rifugia tra le braccia del suo migliore amico, Mario (Massimo Ghini) in partenza per Miami per raggiungere la sua ex moglie e le figlie. Decide quindi di raggiungerlo in Florida nel tentativo di dimenticare la moglie Daniela (Raffaella Bergè). Folle di gelosia per la moglie fuggita con un altro, Giorgio sarà oggetto di spudorate seduzioni da parte di Stella, figlia di Mario, da sempre innamorata di lui. ', 'Massimo Boldi, Christian De Sica', 15),
(94, 'Basic Instinct', 'basic_instinct.jpg', '14.90', 0, 2000, 'USA', 110, 'erotici', 'Paul Verhoeven', 'Nick, un poliziotto di San Francisco, viene incaricato ad indagare sull''omicidio di un cantante rock, ucciso durante un amplesso. I sospetti cadono subito su una bionda e affascinante scrittrice di romanzi gialli, che aveva descritto con minuzia di particolari il delitto in un suo libro prima che ciò accadesse. Ma tra la conturbante Catherine e il poliziotto nasce una passione fatale che metterà in crisi la coscienza e la professionalità dell''investigatore...', 'S.Stone, M.Douglas', 25),
(95, 'Eros', 'eros.jpg', '19.90', 0, 2012, 'ITALIA', 114, 'erotici', 'Wong kar-wai', '"Eros" è un''antologia di tre cortometraggi aventi per soggetto l''erotismo e l''amore diretti dai tre registi più straordinari del mondo, Wong Kar Wai, Steven Soderbergh, e Michelangelo Antonioni. "La Mano": un apprendista sarto, è sedotto dalle misure del corpo di Miss Hua. "Equilibrium": un pubblicitario è ossessionato da un sogno ricorrente. "Il filo pericoloso delle cose": una coppia in crisi, decide di fare una breve gita al mare per ritrovare...', 'L.Ranieri, C.Buchholz', 25),
(96, 'Help me Eros', 'help_me_eros.jpg', '19.90', 0, 2010, 'ITALIA', 121, 'erotici', 'Lee Kang-sheng', 'Ah Jie ha perso tutto in borsa a causa di una grave crisi economica e trascorre le giornate rinchiuso nel suo appartamento a fumare canne e a occuparsi delle piante di marijuana che coltiva in segreto nel suo armadio. Disperato, chiama un numero verde contro i suicidi e conosce Chyi, la cui voce dolce e gentile lo fa innamorare dell%u2019immagine che nella fantasia si è fatto di lei. Folgorato dalla voce della donna, Ah Jie inizia ad avere su di lei delle fantasie che però proietta su Shin, la nuova ragazza che lavora alla bancarella di noci di betel giù in strada. Shin indossa sempre vestiti succinti per attirare i clienti maschi. Lui entra in intimità con lei e ben presto i due sprofondano in un mondo di piaceri erotici e psichedelici...', 'L.Kang, J.Liao', 25),
(97, 'Lovelace', 'lovelace.jpg', '19.90', 0, 2009, 'USA', 122, 'erotici', 'R.Epstein', 'La vera storia di Linda Lovelace, al secolo Linda Boreman, dalla sua ascesa come pornostar negli anni Settanta, quando fu protagonista dell''epocale "Gola profonda", alla presa di posizione contro l''industria del porno. ', 'J.Temple, S.Stone', 25),
(98, 'Sex and Zen', 'sex_and_zen.jpg', '9.90', 0, 2006, 'USA', 125, 'erotici', 'K.Kong-sheng', 'Wei Yangshang, insoddisfatto dal matrimonio con la moglie Tie Yuxiang, inizia a frequentare l''affascinante universo del principe di Ning, costellato di belle e preziose rarità, ma fatto anche di spregiudicati e insaziabili incontri sessuali. Completamente assorbito dal licenzioso mondo di Ning, il giovane Wei Yangshang arriverà a trascurare la casa e la moglie, che chiederà il divorzio, ma resterà invischiato in una trappola mortale...', 'L.Xiao, J.Chan', 25),
(99, 'Verso il Sud', 'verso_il_sud.jpg', '9.90', 0, 2009, 'USA', 120, 'erotici', 'L.Cantent', 'Haiti, anni Ottanta. Brenda , Ellen e Sue tre donne americane in vacanza, si invaghiscono del giovane Legba, bellezza locale che offre loro particolari servigi. L''ambiguo legame che si stabilisce, farà scoprire alle ignare turiste americane l''altra faccia di Haiti, fatta di sfruttamento povertà e violenza, sul finire della terribile dittatura di Jean Claude Duvalier.', 'L.Portal, K.Young', 25),
(100, 'Memorie di una Geisha', 'memorie_di_una_geisha.jpg', '29.90', 0, 2012, 'USA', 125, 'erotici', 'R.Marshall', 'La piccola Chiyo, a soli 9 anni viene venduta ad una scuola per geishe di Kyoto, dove viene istruita sui riti, le danze, la musica, la cerimonia del tè e l''abbigliamento adatto. Costretta a subire vessazioni e umiliazioni dalle colleghe e soprattutto dalla geisha più importante, Hatsumomo, dopo un tentativo di fuga viene retrocessa a serva. A salvarla ci penserà Mameha, geisha esperta e generosa, che la prenderà sotto la sua protezione..', 'K.Momoi,  G.Li', 25),
(101, 'Rischiose Abitudini', 'rischiose_abitudini.jpg', '29.90', 0, 2012, 'USA', 130, 'erotici', 'S.Friars', 'Los Angeles,unica regola: sopravvivere. Tre vite a confronto in una lotta spietata in cui la posta in gioco è il denaro e chi perde...Roy è un "grifter", un ragazzo-fuori: dalla società, dalle regole, dai sentimenti. Myra la sua amante, è una come lui, "esca sexy" senza scrupoli che vede solo il colore dei soldi. Lilly è la madre di Roy. Un passato da non far conoscere, un presente di scommesse e di truffe. Quando tipi cosi hanno le stesse Rischiose Abitudini, allora è una lunga caduta nell''inferno...', 'J.Cusack, A.Aston', 25),
(102, 'Il gusto dell`anguria', 'il_gusto_dell`anguria.jpg', '19.90', 0, 2011, 'USA', 135, 'erotici', 'Tsai Ming', 'A Taipei, in tempo di grave siccità, la televisione cerca di suggerire diversi metodi per risparmiare acqua, come per esempio bere succo d''anguria. Ma ognuno ha i suoi metodi per procurarsi l''acqua e ognuno è come una nuvola solitaria, senza contatti con gli altri...', 'L.Kang, C.Chyi', 50),
(103, 'Luna di fiele', 'luna_di_fiele.jpg', '19.90', 0, 2010, 'USA', 100, 'erotici', 'R.Polansky', 'Di quanto si può oltrepassare il limite per sfuggire alla routine della vita? Su una nave da crociera in rotta verso l''oriente si intrecciano i destini di due coppie in crisi in un crescendo che li porterà verso un epilogo insospettabile... La storia torbida e intensa di un ossessione erotica firmata da un grande regista.', 'H.Grant, E.Seigner', 50),
(104, 'Una sconfinata giovinezza', 'una_sconfinata_giovinezza.jpg', '9.90', 0, 2011, 'USA', 101, 'erotici', 'Pupi Avati', 'Lino Settembre e sua moglie Chicca conducono una vita coniugale serena e senzaserie difficoltà. Sono entrambi soddisfatti delle loro professioni, lui prima firma alla redazione sportiva del Messaggero e lei docente di Filologia Medievale alla Gregoriana. L''unico vero dispiacere che ha accompagnato i venticinque anni di matrimonio è la mancanza di figli. Una mancanza che non ha compromesso la loro unione ma l''ha al contrario rinsaldata.', 'F.Neri, N.Bentivoglio', 50),
(105, 'Vertigini', 'vertigini.jpg', '29.90', 0, 2012, 'ITALIA', 100, 'erotici', 'Tinto Brass', 'Un film sull''incontro tra un uomo anziano e una donna giovane. ', 'G.Shent', 50),
(106, 'Gamer', 'gamer.jpg', '19.90', 0, 2005, 'USA', 109, 'fantascienza', 'B.Taylor', 'Kable è il miglior partecipante di un perverso gioco di ruolo, in cui la tecnologia controlla le menti dei giocatori, che cercherà di uscire dalla gara sconfiggendone lo stesso creatore e controllore.', 'G.Butler, M.Hall', 50);
INSERT INTO `prodotti` (`id`, `titolo`, `immagine`, `prezzo`, `rank`, `anno`, `nazione`, `durata`, `genere`, `regia`, `trama`, `attori`, `quantita`) VALUES
(107, 'Alien La Clonazione', 'alien_la_clonazione.jpg', '9.90', 0, 2007, 'USA', 110, 'fantascienza', 'R.Scott', 'Nello spazio nessuno può sentirti urlare! L''equipaggio di una nave spaziale trova su un pianeta sconosciuto uno strano essere a forma di granchio. Lo portano a bordo e l''alieno comincia a menare strage fra gli astronauti. Il comandante chiede aiuto alla base ma scopre che laggiù sono molto più interessati all''alieno che alla sorte dei terrestri...', 'J.Hurt, H.Stanton', 50),
(108, 'Antiviral', 'antiviral.jpg', '19.90', 0, 2007, 'USA', 111, 'fantascienza', 'B.Cronenberg', 'Syd March lavora per una clinica che rivende sieri di virus, estratti dalle malattie che hanno colpito personaggi famosi e destinati a fan morbosi e ossessionati dalle celebrità. Spinto dalla possibilità di lauti guadagni, Syd usa il proprio corpo come veicolo per rivendere i sieri in maniera clandestina. Quando però si infetta con il virus che ha provocato la morte della popolare Hannah Geist, Syd diviene a sua volta oggetto di mira di collezionisti, criminali e fan. Per evitare una morte certa, l''uomo dovrà svelare la natura misteriosa del virus e trovarvi un antidoto. ', 'D.Smith, S.Gadon', 50),
(109, 'Atto di Forza', 'atto_di_forza.jpg', '25.90', 0, 2012, 'USA', 134, 'fantascienza', 'P.Verhoeven', 'Per Doug Quaid, Marte è sempre il pianeta dei suoi sogni... L''unica via d''uscita è entrare nell''incubo,raggiungere quel pianeta e rispondere all''unica domanda: se non sono io...allora chi sono? Se qualcuno riesce a condizionarti, per i suoi scopi,allora è necessario un atto di forza!', 'A.Schwarzenegger, M.Ironside', 50),
(110, 'Corto Circuito', 'corto_circuito.jpg', '19.90', 0, 2011, 'USA', 132, 'fantascienza', 'J.Bhadham', 'Numero 5, costoso robot pensato come arma da guerra, viene accidentalmente colpito da un fulmine che gli dona la vita. In fuga dall''esercito, che lo vorrebbe disassemblare, Numero 5 trova l''aiuto della bella Stephanie Speck e dei suoi due programmatori.', 'A.Shelly, S.Guttenberg', 25),
(111, 'Deep Impact', 'deep_impact.jpg', '9.90', 0, 2010, 'ENGLAND', 110, 'fantascienza', 'M.Lieder', 'Trovandosi di fronte un ex ministro che reagisce nervosamente alle sue domande, la giornalista Jenny Lerner crede di essere sulle tracce di un grosso scandalo rosa. Mentre insiste nell''indagine, viene fermata e condotta ad un incontro segreto con Tom Beck, presidente degli Stati Uniti, che le preannuncia quello che dirà due giorni dopo in una conferenza stampa: una cometa è in rotta di collisione con la Terra, il rischio è quello di una catastrofe, l''alternativa quella di una difficile missione spaziale che verrà intrapresa per deviare il percorso della cometa. Spurgeon Keeney, astronauta in pensione, riceve il comando della navicella Messia col compito, con i suoi collaboratori, di raggiungere la cometa, atterrare sulla sua superficie e piazzare delle cariche di esplosivo nucleare nella speranza di distruggerla o modificarne l''orbita.', 'E.Good, Tea Leoni', 25),
(112, 'Gravity ', 'gravity.jpg', '14.90', 0, 2009, 'USA', 104, 'fantascienza', 'A.Cuaron', 'Due astronauti, un uomo e una donna, si imbarcano in un disperato viaggio verso casa, dopo che la loro remota base spaziale è stata distrutta da un meteorite. ', 'S.Bullock, G.Clooney', 25),
(113, 'Alexander', 'alexander.jpg', '9.90', 0, 2012, 'USA', 105, 'guerra', 'O.Stone', 'Il giovane Alessandro Magno, desideroso di gloria e avventura, parte dalla Macedonia per conquistare ed estendere il suo dominio portando il suo esercito in zone dove fino ad allora mai nessun occidentale si era spinto. Portava dentro di sé il bisogno di ottenere l''approvazione del padre Filippo, di superare le difficoltà con l''ambiziosa moglie Roxane e contare sul sostegno del grande amico Efestione. Insieme al generale Tolomeo non viene mai battuto in guerra, cosa mai successa nella storia militare. Alessandro e i suoi uomini si spingono attraverso deserti, montagne, lottando contro chiunque osi ostacolare il loro cammino...', 'A.Jolie, C.Farrell', 25),
(114, 'Adam Resurrected', 'adam_resurrected.jpg', '19.90', 0, 2012, 'GERMANIA', 105, 'guerra', 'P.Schrader', 'Israele, 1961. Adam Stein (Jeff Goldblum) è uno dei pazienti dell''istituto mentale per i sopravvissuti all''Olocausto. Prima della guerra, a Berlino, er a un artista molto amato dal pubblico. Poi il campo di concentramento a cui sopravvive diventando ''il cane'' del comandante del campo, intrattenendolo mentre sua moglie e sua figlia sono mandate a morire nelle camere a gas. Un giorno, nell''ospedale, Adam "annusa" un altro "cane", un giovane cresciuto chiuso in una cantina, legato ad una catena. I due si riconoscono per quello che sono e iniziano un percorso di crescita e cura comune.', 'D.Jakobi, M.Bleibtreau', 50),
(115, 'Bloody Sunday', 'bloody_sunday.jpg', '19.90', 0, 2011, 'IRELAND', 110, 'guerra', 'P.Greengrass', 'Irlanda del Nord, Derry: la giornata che segnò l''inizio di una lunga e sofferta guerra civile. Una manifestazione terminata nel sangue, un esercito che difese la legge della violenza. Il racconto di quella giornata e le testimonianze dei protagonisti, in un film per non dimenticare le ingiustizie di ieri e per costruire la giustizia di domani...', 'J.Nesbitt, T.Smith', 50),
(116, 'Buffalo Soldiers', 'buffalo_soldiers.jpg', '14.90', 0, 2010, 'USA', 111, 'guerra', 'G.Jordan', 'Ray Elwood viene condannato a prestare il servizio militare se vuole evitare qualche anno di prigione. Arriva così in una base americana stanziata in Germania e viene messo alle dipendenze del colonnello Berman...', 'Ed Harris, A.Paquin', 25),
(117, 'Dear John', 'dear_john.jpg', '14.90', 0, 2012, 'USA', 112, 'guerra', 'L.Hallstrom', 'John vive con il padre in un piccolo paese della Carolina del Nord. Ha un''adolescenza ribelle e burrascosa e il rapporto con il padre non è dei migliori; quest''ultimo ha una vita molto monotona ed è un grande appassionato di numismatica. Per controllare la sua rabbia, John decide di arruolarsi nell''esercito e viene mandato di stanza in Germania. Durante una licenza incontra Savannah, una giovane studentessa, dolce, carina e molto religiosa. Scatta subito il classico colpo di fulmine, ma dopo una settimana John deve ripartire. l momento di salutarsi lui le annuncia che manca poco al suo congedo: la loro felicità è alle stelle pensando che tra pochi mesi saranno finalmente insieme per sempre, ma l''11 settembre gli Stati Uniti, sono colpiti da un grave attentato e John decide di riconfermare la ferma per altri due anni..', 'H.Thomas,C.Thatum', 25),
(118, 'Full Metal Jacket', 'full_metal_jacket.jpg', '19.90', 0, 2011, 'USA', 124, 'guerra', 'S.Kubrick', 'Un gruppo di marines americani, reclute normali presto trasformate in macchine per uccidere dall''intenso e implacabile addestramento del feroce sergente Hartman, parte per il Vietnam e sperimenta nella cruda offensiva del Tet, che ha per teatro la città vietnamita di Hue, gli orrori di una guerra micidiale per entrambi gli schieramenti, da cui i superstiti non usciranno vincitori, né vinti, ma disumani e cinici professionisti di morte.', 'A.Baldwin, V. D onofrio', 25),
(119, 'Il sangue dei vinti', 'il_sangue_dei_vinti.jpg', '14.90', 0, 2010, 'ITALIA', 127, 'guerra', 'M.Soavi', 'Italia, 19 luglio 1943. Mentre gli alleati bombardano lo scalo ferroviario di San Lorenzo facendo migliaia di vittime, il commissario Franco Dogliani (Michele Placido) indaga sulla morte di una prostituta, Costantina (Barbara Bobulova), uccisa con un colpo d''arma da fuoco nel suo appartamento romano. Il delitto sembra interessare anche le autorità che fanno di tutto per ostacolare le ricerche del poliziotto. Nel frattempo il fratello di Dogliani, Ettore (Alessandro Preziosi), si lega ai partigiani mentre la sorella Lucia (Alina Nedelea) decide di arruolarsi nella X Mas.', 'A.Preziosi, B.Bobulova', 25),
(120, 'Lebanon', 'lebanon.jpg', '14.90', 0, 2009, 'ISRAELE', 135, 'guerra', 'S.Maoz', 'Prima guerra del Libano, giugno 1982. Un carro armato e un plotone di paracadutisti vengono inviati a perlustrare una cittadina ostile bombardata dall''aviazione israeliana. Ma i militari perdono il controllo della missione, che si trasforma in una trappola mortale. Quando scende la notte i soldati feriti restano rinchiusi nel centro della città, senza poter comunicare con il comando centrale e circondati dalle truppe d''assalto siriane che avanzano da ogni lato.', 'I.Donat, T.Banin', 25),
(121, 'Platoon', 'platoon.jpg', '19.90', 0, 2008, 'USA', 156, 'guerra', 'O.Stone', 'Il film più acclamato del 1987, vincitore di 4 premi Oscar, tra cui il miglior film e 3 Globi d''Oro. L''implacabile e straziante "Platoon" ci descrive momento per momento l''impatto che la recluta Chris Taylor (Charlie Sheen) ha con l''infernale guerra del Vietnam mentre prova tutte le emozioni negative possibili. Dalla cieca rabbia alla pazzia, dalla disperazione al terrore, dal sangue alla violenza. Con lui i Sergenti Elias (Willem Dafoe) e Barnes (Tom Berenger).', 'W.Dafoe, K.Dillon', 75),
(122, 'Rescue Dawn', 'rescue_dawn.jpg', '19.90', 0, 2012, 'ENGLAND', 155, 'guerra', 'W.Herzog', 'Il pilota dell''aviazione statunitense Dieter Dengler, americano di origini tedesche, fu abbattuto e catturato in Laos nel corso della guerra del Vietnam. Sopravvisuto al terribile impatto, durante la prigionia, Dengler riuscì ad organizzare la sua fuga e quella di uno sparuto gruppo di prigionieri.', 'C.Bale, S.Zahn', 75),
(123, 'Seraphine', 'seraphine.jpg', '14.90', 0, 2012, 'FRANCIA', 150, 'guerra', 'M.Provost', 'Senlis 1913. Una sguattera, che per una ricca famiglia del luogo svolge le più umilianti faccende domestiche, si rintana nella notte nella sua piccola mansarda. Ha preso del sangue in una macelleria, scavato la terra da un piccolo corso d''acqua, raccolto bacche dai cespugli del bosco. E'' una speziale? Una strega che usa quelle sostanze per i suoi riti magici? Niente di tutto ciò, Seraphine è una pittrice, autodidatta, che utilizza la terra, il sangue, le bacche per dipingere i suoi quadri. La vera storia di un''antisignana dell''arte Naif, Seraphine De Senlis.', 'Y.Moreau', 75),
(124, 'Un homme qui crie', 'un_homme_qui_crie.jpg', '19.90', 0, 2012, 'BELGIO', 111, 'guerra', 'M.Haroun', 'Ciad, ai giorni nostri. Il sessantenne Adam, ex campione di nuoto è ora bagnino in una piscina di un albergo di lusso a N''Djamena. Quando l''albergo viene ricomprato da una società cinese, Adam è costretto a lasciare il posto al figlio Abdel. Adam vive malissimo questa nuova situazione che considera un fallimento sociale e intanto il paese è in preda alla guerra civile...', 'S.Djaorau', 75),
(125, 'Alone in the dark', 'alone_in_the_dark.jpg', '29.90', 0, 2012, 'USA', 121, 'horror', 'U.Bowl', 'Edward Carnby, detective del paranormale, si trova ad indagare sulla misteriosa morte di un suo amico; l''indagine lo porta sull''Isola dell''ombra, dove affronterà prove terribili...', 'C.Slayter, T.Reid', 75),
(126, 'Blood story', 'blood_story.jpg', '19.90', 0, 2004, 'USA', 122, 'horror', 'M.Reeves', 'Owen è un ragazzino che viene costantemente tormentato dai bulli a scuola, e si sente un outsider. Un giorno, fa conoscenza con Abby, la sua nuova vicina di casa. Una ragazzina strana e diversa, come lui. Ma sotto c''è ben di più: Abby è in realtà una vampira...', 'C.Moretz, R.Jenkins', 75),
(127, 'Body', 'body.jpg', '9.90', 0, 2005, 'USA', 125, 'horror', 'J.Lowe', 'Un essere umano contiene 5 litri di sangue, 6 chili di pelle, 206 ossa, 600 muscoli, e 35 milioni di ghiandole. Servono 25 anni di vita al corpo umano per far crescere queste cose.Ma un uomo crede di poter egli stesso liberarsi di ogni singolo pezzo di carne umana grazie all''aiuto di forbici e di una piccola lama chirurgica. E sta per dimostrarlo.Chon inizia a vedere uno psicoterapeuta dopo aver iniziato a sognare una donna incontrata solo una volta in un ristorante. Nei suoi sogni, un uomo misterioso la uccide, taglia a strisce il suo corpo, e ne rovescia i resti uno per uno nella tazza del water.I sogni omicidi lo perseguitano anche da sveglio, e alla fine sente come se il suo corpo fosse stato sezionato e scolpito dal bisturi. Lentamente, quelle immagini terrificanti prendono il controllo su di lui.Arriva così a credere che la donna morta gli stia mandando dei messaggi attraverso questi incubi. Ma non sono messaggi per lui, sono diretti al suo assassino. E parlano di vendetta: "Sono ancora qui." ', 'J.Lowe', 50),
(128, 'Dark Shadows', 'dark_shadows.jpg', '19.90', 0, 2012, 'USA', 145, 'horror', 'T.Burton', 'Nel 1752 Joshua e Naomi Collins, insieme al figlioletto Barnabas, salpano da Liverpool, in Inghilterra, in cerca di una nuova vita negli Stati Uniti. Ma neppure l''oceano è sufficiente a sfuggire alla maledizione che ha colpito la loro famiglia. Due decenni dopo Barnabas si è sistemato nella cittadina di Collinsport, Maine. L''uomo è un ricco playboy, ma compie l''errore fatale di spezzare il cuore della persona sbagliata, la strega Angelique Brouchard, che lo trasforma in vampiro e lo fa seppellire. Dopo duecento anni la tomba di Barnabas viene riavvertitamente aperta liberando il vampiro nel 1972, in un mondo molto diverso da quello di cui aveva memoria. Barnabas fa ritorno a Collinwood Manor per scoprire che la sua casa è andata in rovina, ma che gli eredi della sua eccentrica famiglia vi risiedono ancora. Per risolvere i problemi di famiglia la matriarca Elizabeth Collins Stoddard ha addirittura ingaggiato una pschiatra, la dottoressa Julia Hoffman, che dovrà vedersela con il fratello di Elizabeth Roger Collins, con la figlia adolescente Carolyn Stoddard e col precoce erede di Roger, il piccolo David Collins.', 'M.Pfeiffer, C.Moretz', 50),
(129, 'Doppia personalita''', 'doppia_personalita.jpg', '14.90', 0, 2009, 'USA', 144, 'horror', 'B.De Palma', 'Nella mente di uno psicologo affermato riaffiorano i torbidi ricordi dei sadici esperimenti ai quali, da bambino, era stato sottoposto dal padre psichiatra. ', 'J.Lithgow', 50),
(130, 'Il rito', 'il_rito.jpg', '9.90', 0, 2010, 'USA', 155, 'horror', 'M.Halfstrom', 'Cronaca della bizzarra vita di Padre Gary Thomas, un moderno esorcista che ha preso parte a otto riti ed è stato addestrato a Roma per riconoscere una possessione vera da una malattia mentale. Inizialmente riluttante, Thomas ha poi imparato ad accettare la realtà di questi fenomeni, diventandone un esperto e stravolgendo così la sua stessa visione del mondo.', 'A.Hopkins, A.Braga', 50),
(131, 'Insidious', 'insidious.jpg', '9.90', 0, 2011, 'USA', 175, 'horror', 'J.Wan', 'La storia segue una giovane famiglia che fa la terrificante scoperta che il corpo del loro ragazzo in coma è diventato una calamita per le entità maligne, mentre la sua coscienza si trova intrappolata nel regno oscuro e insidioso conosciuto col nome di The Further. ', 'R.Byrne, P.Wilson', 25),
(132, 'Abduction', 'abduction.jpg', '9.90', 0, 2011, 'USA', 154, 'thriller', 'J.Singleton', 'Nathan (Taylor Lautner) è un ragazzo che da tempo ha dei problemi con i genitori. Un giorno, navigando su internet, scopre una sua foto da bambino pubblicata su un sito dedicato alle persone scomparse, capendo così che quelli che l''hanno cresciuto non sono il suo vero padre e la sua vera madre. Una scoperta che scatenerà una serie di eventi drammatici e violenti. ', 'M.Bello, T.Lautner', 25),
(133, 'Baby Call', 'baby_call.jpg', '19.90', 0, 2012, 'NORWAY', 127, 'thriller', 'P.Sletaune', 'Anna (Noomi Rapace, nuova diva del cinema scandinavo: è la hacker Lisbeth Salander nei film tratti dalla trilogia Millennium dello scrittore svedese Stieg Larsson) e suo figlio Anders (Vetle Qvenild Werring), di 8 anni, per sfuggire al padre violento del bambino si trasferiscono in un luogo segreto, all''interno di un enorme condominio. Anna teme che il suo ex marito possa trovarli e compra un Babycall, affinchè Anders sia al sicuro mentre dorme e lei possa ascoltarne suoni e rumori dall''altra stanza. Dall''apparecchio, però, echeggiano strani gemiti che sembrano provenire da altre parti dell''edificio: Anna ascolta quello che crede sia l''omicidio di un bambino. Nel frattempo, Anders incontra una misteriosa presenza infantile che appare e scompare. Sa qualcosa dei suoni provenienti dal Babycall? Perchè c''è del sangue su un disegno di Anders? Madre e figlio sono ancora in pericolo? ', 'N.Rapace', 25),
(134, 'Blow Out', 'blow_out.jpg', '14.90', 0, 2012, 'USA', 115, 'thriller', 'B.De Palma', 'Jack è un tecnico del suono, ed una notte sta registrando suoni che gli servono per il sonoro di un film dell''orrore di serie B al quale sta lavorando. Accidentalmente registra uno sparo e da quel momento per lui saranno guai... ', 'J.Travolta, N.Allen', 50),
(135, 'Appaloosa', 'appaloosa.jpg', '19.90', 0, 2001, 'USA', 116, 'western', 'Ed Harris', 'Virgil Cole ed Everett Hitch vengono assoldati dagli abitanti di una cittadina senza legge che devono difendersi dai soprusi di Randall Bragg, proprietario terriero e uomo senza scrupoli. Tuttavia, l''arrivo di un''attraente vedova scombussolerà i piani dei due amici...', 'Ed Harris, J.Irons', 75),
(136, 'Batch Cassidy', 'batch_cassidy.jpg', '14.90', 0, 2000, 'USA', 120, 'western', 'G.R.Hill', 'Butch Cassidy e Sundance Kid, amici inseparabili, con una piccola banda assalgono e derubano le banche delle zone vicine al loro rifugio, quindi si ritirano in una casetta dove li attende Etta, una giovane maestrina che, innamorata di Kid, li accudisce soffrendo per la loro vita disperata. Dopo aver derubato un treno della "Union Pacific", saputo che la compagnia li attende al varco, essi sfidano la minaccia assalendo un altro convoglio. Ma mentre stanno raccogliendo il bottino, alcuni uomini della compagnia li sorprendono senza riuscire però a fermarli. Butch e Kid riescono a fuggire, inseguiti per giorni interi finché, ormai circondati, con un atto spericolato evitano la cattura. Sapendosi braccati, decidono di espatriare e raggiungono la Bolivia dove continuano le loro gesta, favoriti anche dalla scarsa efficienza della polizia locale. Decisi a porre fine alla loro attività, lasciano partire Etta e quini fanno il loro ultimo colpo derubando un carico di denaro destinato alle miniere.', 'R.Redford, P.Newman', 25),
(137, 'Giu la testa', 'giu_la_testa.jpg', '9.90', 0, 2000, 'ITALIA', 121, 'western', 'S.Leone', 'Messico, 1916. Un irlandese che vuole combattere la dittatura a fianco di Zapata e Villa, si aggrega ad una banda con la scusa di rapinare una banca. Alla fine riuscirà a convertire il capo della banda alla causa rivoluzionaria.', 'J.Coburn', 50),
(138, 'Gli spietati', 'gli_spietati.jpg', '14.90', 0, 2010, 'USA', 194, 'western', 'C.Eastwood', 'Sono già trascorsi undici anni da quando William Munny ha riposto il fucile e si è ritirato, ma i tempi sono duri. La moglie è morta, i bambini sono affamati e il bestiame decimato dalla febbre. Si presenta alla fattoria Schofield Kid, un giovane cacciatore di taglie alla ricerca di un socio. E chi meglio del famigerato pistolero William Munny?', 'C.Eastwood, J.Hackman', 25),
(139, 'Gli spostati', 'gli_spostati.jpg', '14.90', 0, 2000, 'USA', 166, 'western', 'J.Huston', 'Roslyn è a Reno per divorziare. Perce, Guido e Gay invece a Reno ci vivono, catturando cavalli selvaggi e partecipando a rodei. Tutti e tre sono attratti dalla bella Roslyn. Durante una caccia ai mustang, Perce si fa male e Roslyn ne è sconvolta: quel mondo duro e violento le fa vedere sotto un diverso aspetto Gay. Così quando Guido e Gay partecipano ad una nuova caccia a un branco di cavalli selvaggi, aiutata da Perce, la donna libera l''animale che i due hanno con tanta fatica catturato. Gay allora lo ricattura, per liberarlo però a sua volta, in omaggio a Roslyn... ', 'M.Monroe, E.Wallack', 25),
(140, 'Il buono il matto il cattivo', 'il_buono_il_matto_il_cattivo.jpg', '19.90', 0, 2007, 'KOREA', 110, 'western', 'J.Kim', 'Manciuria, anni ''30. Chang-yi, killer dandy che ha perennemente stampato in faccia un sogghigno ironico, viene assoldato per recuperare una mappa in mano a un banchiere giapponese. Intanto, a sua insaputa, l''infallibile cacciatore di taglie Do-won viene incaricato della stessa missione dall''esercito indipendentista coreano. La mappa però, dopo un''esplosiva rapina ad un treno, è finita nelle mani di Tae-gu, ladro convinto che essa conduca ai favolosi tesori della dinastia Qing. Tra i tre si scatenerà una lotta serrata per il possesso della mappa e dei tesori che cela. ', 'B.Lee, J.Wang', 25),
(141, 'La magnifica preda', 'la_magnifica_preda.jpg', '10.90', 0, 2000, 'USA', 105, 'western', 'O.Preminger', 'Matt Carter è un agricoltore che vive con suo figlio. Un giorno salva dal fiume un uomo e una donna: lui gli prende il cavallo e si reca in città per registrare la proprietà di una miniera vinta al gioco, la donna invece resta con Matt... ', 'O.Mitchum, M.Monroe', 25),
(142, 'The killer inside me', 'the_killer_inside_me.jpg', '9.90', 0, 2009, 'USA', 95, 'western', 'M.Winterbottom', 'Lou Ford è il vice sceriffo di una piccola cittadina del Texas. E'' un pilastro della comunità, paziente e apparentemente pensieroso. Alcune persone pensano che sia un po'' lento e forse noioso, ma questa è la cosa peggiore che dicono di lui. Nessuno conosce ancora quella che Lou chiama la sua "malattia", che è stata in letargo per anni e che ora però è in procinto di risalire in superficie con conseguenze brutali e devastanti...', 'E.Koteas, L.Haiken', 50),
(143, 'The lone ranger', 'the_lone_ranger.jpg', '9.90', 0, 2012, 'USA', 104, 'western', 'G.Verbinski', 'Il guerriero indiano Tonto (Johnny Depp) racconta la storia inedita che ha trasformato John Reid (Armie Hammer), un uomo di legge, in un leggendario giustiziere, trasportando il pubblico in un''epica girandola di sorprese in cui i due improbabili eroi, spesso impegnati in comici alterchi, combatteranno fianco a fianco contro l''avidità e la corruzione.', 'J.Depp, T.Wilkinson', 75),
(144, 'Wyatt Earp', 'wyatt_earp.jpg', '9.90', 0, 2012, 'USA', 99, 'western', 'L.Kasdan', 'La storia di Wyatt Earp, lo sceriffo più famoso del West, in un ritratto dai contrasti forti Kevin Costner è rappresentante della legge sia pure al limite della legalità; duro e intransigente ma nel contempo appassionato, sincero nel manifestare i propri sentimenti. La vita, le lotte e gli amori di un uomo che divenne leggenda...', 'K.Costner, J.Caviezel', 75),
(145, 'I cancelli del cielo', 'i_cancelli_del_cielo.jpg', '9.90', 0, 2011, 'USA', 125, 'western', 'M.Cimino', 'Nel Wyoming del 1890, un gruppo di proprietari terrieri si arma per contrastare l''afflusso di immigrati europei che minaccia la loro egemonia sulla terra. Un uomo di legge, James Averill, si oppone alla violenza e aiuta la comunità di immigrati, conducendoli in una battaglia che determinerà il loro destino.', 'J.Cotten, B.Dourif', 50);

-- --------------------------------------------------------

--
-- Struttura della tabella `province`
--

CREATE TABLE IF NOT EXISTS `province` (
  `id_prov` int(16) NOT NULL AUTO_INCREMENT,
  `sigla` varchar(5) NOT NULL,
  `nomep` varchar(128) NOT NULL,
  PRIMARY KEY (`id_prov`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=110 ;

--
-- Dump dei dati per la tabella `province`
--

INSERT INTO `province` (`id_prov`, `sigla`, `nomep`) VALUES
(1, 'AN', 'Ancona'),
(2, 'MC', 'Macerata'),
(3, 'PU', 'Pesaro Urbino'),
(4, 'AP', 'Ascoli Piceno'),
(5, 'AL', 'Alessandria'),
(6, 'AT', 'Asti'),
(7, 'BI', 'Biella'),
(8, 'CN', 'Cuneo'),
(9, 'NO', 'Novara'),
(10, 'VC', 'Vercelli'),
(11, 'TO', 'Torino'),
(12, 'AG', 'Agrigento'),
(13, 'CL', 'Caltanissetta'),
(14, 'CT', 'Catania'),
(15, 'EN', 'Enna'),
(16, 'ME', 'Messina'),
(17, 'PA', 'Palermo'),
(18, 'RG', 'Ragusa'),
(19, 'SR', 'Siracusa'),
(20, 'TP', 'Trapani'),
(21, 'CZ', 'Catanzaro'),
(22, 'CS', 'Cosenza'),
(23, 'KR', 'Crotone'),
(24, 'RC', 'Reggio Calabria'),
(25, 'VV', 'Vibo Valentia'),
(27, 'MT', 'Matera'),
(28, 'PZ', 'Potenza'),
(29, 'BA', 'Bari'),
(30, 'BR', 'Brindisi'),
(31, 'FG', 'Foggia'),
(32, 'LE', 'Lecce'),
(33, 'TA', 'Taranto'),
(34, 'AV', 'Avellino'),
(35, 'BN', 'Benevento'),
(36, 'CE', 'Caserta'),
(37, 'NA', 'Napoli'),
(38, 'SA', 'Salerno'),
(39, 'FR', 'Frosinone'),
(40, 'LT', 'Latina'),
(41, 'RI', 'Rieti'),
(42, 'RM', 'Roma'),
(43, 'VT', 'Viterbo'),
(44, 'CH', 'Chieti'),
(45, 'AQ', 'L''Aquila'),
(46, 'PE', 'Pescara'),
(47, 'TE', 'Teramo'),
(48, 'AR', 'Arezzo'),
(49, 'FI', 'Firenze'),
(50, 'GR', 'Grosseto'),
(51, 'LI', 'Livorno'),
(52, 'LU', 'Lucca'),
(53, 'MS', 'Massa Carrara'),
(54, 'PI', 'Pisa'),
(55, 'PT', 'Pistoia'),
(56, 'SI', 'Siena'),
(57, 'BO', 'Bologna'),
(58, 'FE', 'Ferrara'),
(59, 'FC', 'Forl&iacute; Cesena'),
(60, 'MO', 'Modena'),
(61, 'PR', 'Parma'),
(62, 'PC', 'Piacenza'),
(63, 'RA', 'Ravenna'),
(64, 'RE', 'Reggio Emilia'),
(65, 'RN', 'Rimini'),
(66, 'BL', 'Belluno'),
(67, 'PD', 'Padova'),
(68, 'RO', 'Rovigo'),
(69, 'TV', 'Treviso'),
(70, 'VE', 'Venezia'),
(71, 'VR', 'Verona'),
(72, 'VI', 'Vicenza'),
(73, 'GO', 'Gorizia'),
(74, 'PN', 'Pordenone'),
(75, 'UD', 'Udine'),
(76, 'TS', 'Trieste'),
(77, 'AO', 'Aosta'),
(78, 'CA', 'Cagliari'),
(79, 'NU', 'Nuoro'),
(80, 'OR', 'Oristano'),
(81, 'SS', 'Sassari'),
(82, 'GE', 'Genova'),
(83, 'IM', 'Imperia'),
(84, 'SV', 'Savona'),
(85, 'SP', 'La Spezia'),
(86, 'IS', 'Isernia'),
(87, 'CB', 'Campobasso'),
(88, 'PG', 'Perugia'),
(89, 'TR', 'Terni'),
(90, 'BG', 'Bergamo'),
(91, 'BS', 'Brescia'),
(92, 'CO', 'Como'),
(93, 'CR', 'Cremona'),
(94, 'LC', 'Lecco'),
(95, 'LO', 'Lodi'),
(96, 'MN', 'Mantova'),
(97, 'MI', 'Milano'),
(98, 'PV', 'Pavia'),
(99, 'SO', 'Sondrio'),
(100, 'VA', 'Varese'),
(101, 'TN', 'Trento'),
(102, 'BZ', 'Bolzano'),
(103, 'PO', 'Prato'),
(104, 'VB', 'Verbania'),
(105, 'CI', 'Carbonia Iglesias'),
(106, 'VS', 'Medio Campidano'),
(107, 'OG', 'Ogliastra'),
(108, 'OT', 'Olbia Tempio'),
(109, '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `servizi`
--

CREATE TABLE IF NOT EXISTS `servizi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gruppo` int(11) NOT NULL,
  `link` varchar(50) NOT NULL,
  `nometab` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dump dei dati per la tabella `servizi`
--

INSERT INTO `servizi` (`id`, `gruppo`, `link`, `nometab`) VALUES
(1, 1, 'profilo_cliente.php?tab=dati', 'Dati Utente'),
(2, 1, 'profilo_cliente.php?tab=modificaana', 'Modifica Anagrafica'),
(3, 1, 'profilo_cliente.php?tab=modificamail', 'Modifica Email'),
(4, 1, 'profilo_cliente.php?tab=modificapass', 'Modifica Password'),
(5, 2, 'profilo_cliente.php?tab=dati', 'Dati Utente'),
(6, 2, 'profilo_cliente.php?tab=modificaana', 'Modifica Anagrafica'),
(7, 2, 'profilo_cliente.php?tab=modificamail', 'Modifica Email'),
(8, 2, 'profilo_cliente.php?tab=modificapass', 'Modifica Password'),
(9, 2, 'profilo_cliente.php?tab=additem', 'Aggiungi Prodotto'),
(10, 2, 'profilo_cliente.php?tab=modprod', 'Modifica Prodotto'),
(11, 2, 'profilo_cliente.php?tab=deleteitem', 'Cancella Prodotto'),
(12, 2, 'profilo_cliente.php?tab=addute', 'Aggiungi Utente'),
(13, 2, 'profilo_cliente.php?tab=deleteute', 'Cancella Utente'),
(14, 2, 'profilo_cliente.php?tab=addcat', 'Aggiungi Categoria'),
(15, 2, 'profilo_cliente.php?tab=ordini', 'Ordini'),
(16, 3, 'profilo_cliente.php?tab=dati', 'Dati Utente'),
(17, 3, 'profilo_cliente.php?tab=modificaana', 'Modifica Anagrafica'),
(18, 3, 'profilo_cliente.php?tab=modificamail', 'Modifica Email'),
(19, 3, 'profilo_cliente.php?tab=modificapass', 'Modifica Password'),
(20, 3, 'profilo_cliente.php?tab=ordini', 'Ordini');

-- --------------------------------------------------------

--
-- Struttura della tabella `stati`
--

CREATE TABLE IF NOT EXISTS `stati` (
  `id_stati` int(11) NOT NULL AUTO_INCREMENT,
  `sigla` varchar(5) NOT NULL,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id_stati`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=241 ;

--
-- Dump dei dati per la tabella `stati`
--

INSERT INTO `stati` (`id_stati`, `sigla`, `nome`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'AS', 'Samoa Americane'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antartide'),
(9, 'AG', 'Antigua e Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrein'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Bielorussia'),
(21, 'BE', 'Belgio'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia Erzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Isola Bouvet'),
(30, 'BR', 'Brasile'),
(31, 'IO', 'Oceano Indiano, territorio britannico'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambogia'),
(37, 'CM', 'Camerun'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Capo Verde'),
(40, 'KY', 'Isole Cayman'),
(41, 'CF', 'Repubblica Centrafricana'),
(42, 'TD', 'Ciad'),
(43, 'CL', 'Cile'),
(44, 'CN', 'Cina'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comore'),
(49, 'CG', 'Congo'),
(50, 'CD', 'Congo,Rep. Democratica'),
(51, 'CK', 'Isole di Cook'),
(52, 'CR', 'Costa Rica'),
(53, 'CI', 'Costa d''Avorio'),
(54, 'HR', 'Croazia'),
(55, 'CU', 'Cuba'),
(56, 'CY', 'Cipro'),
(57, 'CZ', 'Repubblica Ceca'),
(58, 'DK', 'Danimarca'),
(59, 'DJ', 'Gibuti'),
(60, 'DM', 'Dominica'),
(61, 'DO', 'Repubblica Dominicana'),
(62, 'TP', 'Timor Est'),
(63, 'EC', 'Ecuador'),
(64, 'EG', 'Egitto'),
(65, 'SV', 'El Salvador'),
(66, 'GQ', 'Guinea Equatoriale'),
(67, 'ER', 'Eritrea'),
(68, 'EE', 'Estonia'),
(69, 'ET', 'Etiopia'),
(70, 'FK', 'Falkland Islands (Malvinas)'),
(71, 'FO', 'Isole Faroe'),
(72, 'FJ', 'Figi'),
(73, 'FI', 'Finlandia'),
(74, 'FR', 'Francia'),
(75, 'GF', 'Guiana Francese'),
(76, 'PF', 'Polinesia Francese'),
(77, 'TF', 'Territori Francesi del Sud'),
(78, 'GA', 'Gabon'),
(79, 'GM', 'Gambia'),
(80, 'GE', 'Georgia'),
(81, 'DE', 'Germania'),
(82, 'GH', 'Ghana'),
(83, 'GI', 'Gibilterra'),
(84, 'GR', 'Grecia'),
(85, 'GL', 'Groenlandia'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadalupa'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Isole Heard e McDonald'),
(95, 'VA', 'Holy See (Vatican City State)'),
(96, 'HN', 'Honduras'),
(97, 'HK', 'Hong Kong'),
(98, 'HU', 'Ungheria'),
(99, 'IS', 'Islanda'),
(100, 'IN', 'India'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'EIRE'),
(105, 'IL', 'Israele'),
(106, 'IT', 'Italia'),
(107, 'JM', 'Giamaica'),
(108, 'JP', 'Giappone'),
(109, 'JO', 'Giordania'),
(110, 'KZ', 'Kazakistan'),
(111, 'KE', 'Kenya'),
(112, 'KI', 'Kiribati'),
(113, 'KP', 'Corea del Nord'),
(114, 'KR', 'Corea del Sud'),
(115, 'KW', 'Kuwait'),
(116, 'KG', 'Kirgizistan'),
(117, 'LA', 'Laos'),
(118, 'LV', 'Lettonia'),
(119, 'LB', 'Libano'),
(120, 'LS', 'Lesotho'),
(121, 'LR', 'Liberia'),
(122, 'LY', 'Libia'),
(123, 'LI', 'Liechtenstein'),
(124, 'LT', 'Lituania'),
(125, 'LU', 'Lussemburgo'),
(126, 'MO', 'Macao'),
(127, 'MK', 'Macedonia'),
(128, 'MG', 'Madagascar'),
(129, 'MW', 'Malawi'),
(130, 'MY', 'Malaysia'),
(131, 'MV', 'Isole Maldive'),
(132, 'ML', 'Mali'),
(133, 'MT', 'Malta'),
(134, 'MH', 'Isole Marshall'),
(135, 'MQ', 'Martinica'),
(136, 'MR', 'Mauritania'),
(137, 'MU', 'Mauritius'),
(138, 'YT', 'Mayotte'),
(139, 'MX', 'Mexico'),
(140, 'FM', 'Micronesia'),
(141, 'MD', 'Moldova, Republic of'),
(142, 'MC', 'Monaco'),
(143, 'MN', 'Mongolia'),
(144, 'MS', 'Montserrat'),
(145, 'MA', 'Marocco'),
(146, 'MZ', 'Mozambique'),
(147, 'MM', 'Myanmar'),
(148, 'NA', 'Namibia'),
(149, 'NR', 'Nauru'),
(150, 'NP', 'Nepal'),
(151, 'NL', 'Paesi Bassi'),
(152, 'AN', 'Antille Olandesi'),
(153, 'NC', 'Nuova Caledonia'),
(154, 'NZ', 'Nuova Zelanda'),
(155, 'NI', 'Nicaragua'),
(156, 'NE', 'Niger'),
(157, 'NG', 'Nigeria'),
(158, 'NU', 'Niue'),
(159, 'NF', 'Isola di Norfolk'),
(160, 'MP', 'Isole Marianne del Nord'),
(161, 'NO', 'Norvegia'),
(162, 'OM', 'Oman'),
(163, 'PK', 'Pakistan'),
(164, 'PW', 'Palau'),
(165, 'PA', 'Panama'),
(166, 'PG', 'Nuova Guinea'),
(167, 'PY', 'Paraguay'),
(168, 'PE', 'Peru'),
(169, 'PH', 'Filippine'),
(170, 'PN', 'Pitcairn'),
(171, 'PL', 'Polonia'),
(172, 'PT', 'Portogallo'),
(173, 'PR', 'Porto Rico'),
(174, 'QA', 'Qatar'),
(175, 'RE', 'R&egrave;union'),
(176, 'RO', 'Romania'),
(177, 'RU', 'Russia'),
(178, 'RW', 'Rwanda'),
(179, 'SH', 'Sant''Elena'),
(180, 'KN', 'Saint Kitts e Nevis'),
(181, 'LC', 'Santa Lucia'),
(182, 'PM', 'Saint Pierre e Miquelon'),
(183, 'VC', 'Saint Vincent e le Grenadine'),
(184, 'WS', 'Samoa'),
(185, 'SM', 'San Marino'),
(186, 'ST', 'Sao Tome e Principe'),
(187, 'SA', 'Arabia Saudita'),
(188, 'SN', 'Senegal'),
(189, 'SC', 'Seychelles'),
(190, 'SL', 'Sierra Leone'),
(191, 'SG', 'Singapore'),
(192, 'SK', 'Slovacchia'),
(193, 'SI', 'Slovenia'),
(194, 'SB', 'Isole Solomone'),
(195, 'SO', 'Somalia'),
(196, 'ZA', 'Sud Africa'),
(197, 'GS', 'Sud Georgia e Isole Sandwich del Sud'),
(198, 'ES', 'Spagna'),
(199, 'LK', 'Sri Lanka'),
(200, 'SD', 'Sudan'),
(201, 'SR', 'Suriname'),
(202, 'SJ', 'Svalbard e Jan Mayen'),
(203, 'SZ', 'Swaziland'),
(204, 'SE', 'Svezia'),
(205, 'CH', 'Svizzera'),
(206, 'SY', 'Siria'),
(207, 'TW', 'Taiwan'),
(208, 'TJ', 'Tagikistan'),
(209, 'TZ', 'Tanzania'),
(210, 'TH', 'Thailand'),
(211, 'TG', 'Togo'),
(212, 'TK', 'Tokelau'),
(213, 'TO', 'Tonga'),
(214, 'TT', 'Trinidad e Tobago'),
(215, 'TN', 'Tunisia'),
(216, 'TR', 'Turchia'),
(217, 'TM', 'Turkmenistan'),
(218, 'TC', 'Isole Turks e Caicos'),
(219, 'TV', 'Tuvalu'),
(220, 'UG', 'Uganda'),
(221, 'UA', 'Ucraina'),
(222, 'AE', 'Emirati Arabi Uniti'),
(223, 'GB', 'Gran Bretagna'),
(224, 'US', 'USA'),
(225, 'UM', 'Isole Minor Outlying, USA'),
(226, 'UY', 'Isole Vergini, USA'),
(227, 'UZ', 'Uruguay'),
(228, 'VU', 'Uzbekistan'),
(229, 'VE', 'Vanuatu'),
(230, 'VN', 'Venezuela'),
(231, 'VG', 'Vietnam'),
(232, 'VI', 'Isole Vergini, GB'),
(233, 'WF', 'Wallis e Futuna'),
(234, 'EH', 'Sahara Occidentale'),
(235, 'YE', 'Yemen'),
(236, 'YU', 'Serbia e Montenegro'),
(237, 'ZM', 'Zambia'),
(238, 'ZW', 'Zimbabwe'),
(239, 'PS', 'Territori Palestinesi'),
(240, '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE IF NOT EXISTS `utenti` (
  `nome` varchar(20) NOT NULL,
  `cognome` varchar(50) NOT NULL,
  `via` varchar(50) NOT NULL,
  `citta` varchar(20) NOT NULL,
  `provincia` varchar(15) NOT NULL,
  `cap` int(5) NOT NULL,
  `paese` varchar(20) NOT NULL,
  `telefono` varchar(11) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `data_nasc` date DEFAULT NULL,
  `pass` varchar(15) NOT NULL,
  `CF` varchar(16) NOT NULL,
  `id_gruppo` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`nome`, `cognome`, `via`, `citta`, `provincia`, `cap`, `paese`, `telefono`, `email`, `data_nasc`, `pass`, `CF`, `id_gruppo`) VALUES
('Alfonso', 'Pierantonio', 'Vetoio, 8', '', 'L&#039;Aquila', 67100, '', '3334845123', 'alfonso@gmail.com', '1980-06-07', 'alfonso', 'PRNLNS80I07E055M', 1),
('Francesco', 'Angelini', 'corridoni, 8', '', 'Frosinone', 2048, '', '3357489565', 'angelini@gmail.com', '1990-05-20', 'angelini', 'NGLFNC90H20H878B', 1),
('Francesco', 'Colaiezzi', 'galilei, 5', '', 'Chieti', 66026, '', '3251225540', 'colaiezzi@gmail.com', '1989-10-26', 'colaiezzi', 'CLZFNC89R26G141Z', 3),
('Federico', 'FossemÃ²', 'Australia, 13', 'L&#039;Aquila', 'L&#039;Aquila', 67100, 'Italia', '3280821334', 'fede@gmail.com', '1989-02-10', 'fede', 'FSSFRC89B10E058N', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
